# blog-soidev [![pipeline status](https://gitlab.com/soiboxauxi/blog-soidev/badges/master/pipeline.svg)](https://gitlab.com/soiboxauxi/blog-soidev/-/commits/master)

```bash
docker-compose -f docker-compose.base.yml -f docker-compose.base.override.yml -f docker-compose.yml -f docker-compose.override.yml build
docker-compose -f docker-compose.base.yml -f docker-compose.base.override.yml -f docker-compose.yml -f docker-compose.override.yml up -d
docker-compose -f docker-compose.base.yml -f docker-compose.base.override.yml -f docker-compose.yml -f docker-compose.override.yml down
```