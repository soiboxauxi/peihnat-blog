#!/bin/bash

echo =============== WAITING FOR MSSQL ==========================
#waiting for mssql to start
export STATUS=0
i=0
while [[ $STATUS -eq 0 ]] || [[ $i -lt 90 ]]; do
	sleep 1
	i=$((i+1))
	export STATUS=$(grep 'SQL Server is now ready for client connections' /var/opt/mssql/log/errorlog | wc -l)
done