#!/bin/sh
DOCKER_ENV=''
DOCKER_TAG=''
DOCKER_GROUP=''
echo $CI_DEFAULT_BRANCH

case "$CI_DEFAULT_BRANCH" in
    "master")
    DOCKER_ENV=production
    DOCKER_TAG=latest
    DOCKER_GROUP=soiboxauxi/peihnat-blog
    ;;
esac

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

echo "Build Mssqldb Database"
echo "================================================================================"
docker build -f ./deploys/dockers/mssqldb/Dockerfile -t $CI_REGISTRY/$DOCKER_GROUP/cs-mssqldb:$DOCKER_TAG .
docker push $CI_REGISTRY/$DOCKER_GROUP/cs-mssqldb:$DOCKER_TAG
echo "================================================================================"

echo "Build Post Service"
echo "================================================================================"
docker build -f ./src/Services/PostService/PEIHNAT.PostService.Api/Dockerfile -t $CI_REGISTRY/$DOCKER_GROUP/cs-post-service:$DOCKER_TAG .
docker push $CI_REGISTRY/$DOCKER_GROUP/cs-post-service:$DOCKER_TAG
echo "================================================================================"