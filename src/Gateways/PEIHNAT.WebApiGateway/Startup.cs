using CorrelationId;
using Grpc.Core;
using GrpcJsonTranscoder;
using GrpcJsonTranscoder.Grpc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using OpenTracing.Contrib.Grpc.Interceptors;
using OpenTracing.Util;
using PEIHNAT.Infrastructure.Tracing.Jaeger;
using System;
using System.Linq;
using System.Text;
using static PEIHNAT.PostService.DataContracts.Api.V1.PostApi;

namespace PEIHNAT.WebApiGateway
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorrelationId();
            services.AddJaeger();

            services.AddGrpcJsonTranscoder(() =>
                new GrpcAssemblyResolver().ConfigGrpcAssembly(
                    typeof(PostApiBase).Assembly));

            // only for demo
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            services.AddOcelot();
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            //var configuration = new OcelotPipelineConfiguration
            //{
            //    PreQueryStringBuilderMiddleware = async (ctx, next) =>
            //    {
            //        try
            //        {
            //            var tracer = GlobalTracer.Instance;
            //
            //            if (tracer?.ActiveSpan == null)
            //            {
            //                return;
            //            }
            //
            //            await ctx.HandleGrpcRequestAsync(next, new[] { new ClientTracingInterceptor(tracer) });
            //        }
            //        catch (Exception ex)
            //        {
            //            if (!(ex.InnerException is AggregateException innerEx))
            //                return;
            //
            //            innerEx.InnerExceptions
            //                .Select(async aggException =>
            //                {
            //                    if (aggException is RpcException rpcException)
            //                    {
            //                        if (rpcException.StatusCode == StatusCode.Internal)
            //                        {
            //                            ctx.HttpContext.Response.StatusCode = 500;
            //
            //                            await ctx.HttpContext.Response.Body.WriteAsync(Encoding.UTF8.GetBytes($"{rpcException.Message}"));
            //                        }
            //                        else
            //                        {
            //                            var status = GetTrailerKeyOnRpcException(rpcException, ":status");
            //
            //                            var authMessage = GetTrailerKeyOnRpcException(rpcException, "www-authenticate");
            //
            //                            if (status == "401")
            //                            {
            //                                ctx.HttpContext.Response.StatusCode = 401;
            //
            //                                await ctx.HttpContext.Response.Body.WriteAsync(Encoding.UTF8.GetBytes($"{authMessage}"));
            //                            }
            //                        }
            //                    }
            //                })
            //                .ToList();
            //
            //            throw ex;
            //        }
            //    }
            //};

            app.UseCors("CorsPolicy");

            app.UseCorrelationId();

            //app.UseOcelot(configuration).Wait();
            app.UseOcelot().Wait();
        }

        private static string GetTrailerKeyOnRpcException(RpcException rpcException, string key)
        {
            return rpcException.Trailers?.Select(x =>
            {
                if (x.Key == key)
                    return x.Value;

                return string.Empty;
            })
            .FirstOrDefault(x => !string.IsNullOrEmpty(x));
        }
    }
}
