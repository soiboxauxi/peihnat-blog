using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using N5T.Infrastructure.Tye;
using Microsoft.ReverseProxy.Abstractions;
using Microsoft.ReverseProxy.Service;
using System.Collections.Generic;
using N5T.Infrastructure.Logging;

namespace ApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Config = config;
        }

        private IConfiguration Config { get; }
        private bool IsRunOnTye => Config.IsRunOnTye();

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            
            services.AddCors(options =>
            {
                options.AddPolicy("api", policy =>
                {
                    policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });

            #region Reverse proxy
            
            #region Post
            string postUrl = IsRunOnTye
                ? $"{Config.GetServiceUri("postapp")?.AbsoluteUri}"
                : Config.GetValue<string>("Services:postapp");
            
            ProxyRoute postRoute = new ProxyRoute()
            {
                RouteId = "post",
                ClusterId = "post-svc-cluster",
                Match =
                {
                    Path = "/post/{**catch-all}"
                },
                Transforms = new List<IDictionary<string, string>>()
            };
            
            postRoute.AddTransformXForwarded();
            postRoute.AddTransformPathRemovePrefix("/post");
            
            Cluster postCluster = new Cluster()
            {
                Id = "post-svc-cluster",
                Destinations =
                {
                    {
                        "post-svc-cluster/destination1", new Destination()
                        {
                            Address = postUrl
                        }
                    }
                }
            };
            #endregion
            
            var routes = new[]
            {
                postRoute
            };
            
            var clusters = new[]
            {
                postCluster
            };
            
            services.AddReverseProxy().LoadFromMemory(routes, clusters);
            
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("api");

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    context.Response.ContentType = "text/html";
                    await context.Response.WriteAsync("<h3>ApiGateway</h3>");
                    await context.Response.WriteAsync("<br>");
                    await context.Response.WriteAsync("<a href='/post/info'>Post&nbsp;</a>");
                    await context.Response.WriteAsync("<a href='/inv/info'>Inventory&nbsp;|&nbsp;</a>");
                    await context.Response.WriteAsync("<a href='/prod/info'>Product Catalog&nbsp;|&nbsp;</a>");
                    await context.Response.WriteAsync("<a href='/cart/info'>Shopping Cart&nbsp;|&nbsp;</a>");
                    await context.Response.WriteAsync("<a href='/sale/info'>Sale&nbsp;</a>");
                });

                endpoints.MapReverseProxy(proxyPipeline =>
                {
                    proxyPipeline.UseAffinitizedDestinationLookup();
                    proxyPipeline.UseProxyLoadBalancing();
                    proxyPipeline.UseRequestAffinitizer();
                });
            });

            app.ApplicationServices.CreateLoggerConfiguration(IsRunOnTye);
        }
    }
}
