using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using N5T.Infrastructure;
using N5T.Infrastructure.Helpers;
using System.Diagnostics;

namespace ApiGateway
{
    public class Program
    {
        public static int Main(string[] args)
        {            
            var (hostBuilder, isRunOnTye) = HostHelper.CreateHostBuilder<Startup>(args);
            return hostBuilder.Run(isRunOnTye);
        }
    }
}
