﻿using Microsoft.AspNetCore.Authorization;

namespace N5T.Infrastructure.Auth
{
    public interface IAuthRequest : IAuthorizationRequirement
    {
    }
}
