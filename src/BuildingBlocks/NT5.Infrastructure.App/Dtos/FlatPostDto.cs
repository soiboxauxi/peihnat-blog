﻿using System;

namespace NT5.Infrastructure.App.Dtos
{
    public class FlatPostDto
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
    }
}
