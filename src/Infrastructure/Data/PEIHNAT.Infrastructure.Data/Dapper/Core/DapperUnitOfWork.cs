﻿using PEIHNAT.Domain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PEIHNAT.Infrastructure.Data.Dapper.Core
{
    public interface IDapperUnitOfWork : IUnitOfWork
    {
        ISqlConnectionFactory SqlConnectionFactory { get; }
    }

    public class DapperUnitOfWork : IDapperUnitOfWork
    {
        private ConcurrentDictionary<string, object> _repositories = null;

        public ISqlConnectionFactory SqlConnectionFactory { get; }

        public IEnumerable<IDomainEventDispatcher> EventBuses { get; }

        public DapperUnitOfWork(ISqlConnectionFactory sqlConnectionFactory, IEnumerable<IDomainEventDispatcher> eventBuses)
        {
            SqlConnectionFactory = sqlConnectionFactory;
            EventBuses = eventBuses;
        }

        public void Dispose()
        {
            if (SqlConnectionFactory != null)
            {
                GC.SuppressFinalize(SqlConnectionFactory);
            }
        }

        public int SaveChanges()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        public IQueryRepository<TEntity, TId> QueryRepository<TEntity, TId>() where TEntity : class, IAggregateRoot<TId>
        {
            if (_repositories == null)
                _repositories = new ConcurrentDictionary<string, object>();

            var key = $"{typeof(TEntity)}-query";
            if (!_repositories.ContainsKey(key))
            {
                var cachedRepo = new GenericRepository<TEntity, TId>(SqlConnectionFactory, EventBuses);
                _repositories[key] = cachedRepo;
            }

            return (IQueryRepository<TEntity, TId>)_repositories[key];
        }

        IRepositoryAsync<TEntity, TId> IRepositoryFactory.RepositoryAsync<TEntity, TId>()
        {
            throw new System.NotImplementedException();
        }
    }
}
