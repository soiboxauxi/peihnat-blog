﻿using Dapper;
using PEIHNAT.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PEIHNAT.Infrastructure.Data.Dapper.Core
{
    public class GenericRepository<TEntity, TId> : IRepositoryAsync<TEntity, TId>, IQueryRepository<TEntity, TId>
        where TEntity : class, IAggregateRoot<TId>
    {
        public ISqlConnectionFactory SqlConnectionFactory { get; }
        public IEnumerable<IDomainEventDispatcher> EventBuses { get; }

        public GenericRepository(
            ISqlConnectionFactory sqlConnectionFactory, 
            IEnumerable<IDomainEventDispatcher> eventBuses)
        {
            SqlConnectionFactory = sqlConnectionFactory;
            EventBuses = eventBuses;
        }

        public Task<TEntity> AddAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> Queryable()
        {
            using var conn = SqlConnectionFactory.GetOpenConnection();
            var entities = conn.GetList<TEntity>();
            return entities.AsQueryable();
        }

        public Task<TEntity> UpdateAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
