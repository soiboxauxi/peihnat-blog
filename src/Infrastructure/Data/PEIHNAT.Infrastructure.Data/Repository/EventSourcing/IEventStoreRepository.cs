﻿using PEIHNAT.Domain.Core.Events;
using System;
using System.Collections.Generic;

namespace PEIHNAT.Infrastructure.Data.Repository.EventSourcing
{
    public interface IEventStoreRepository : IDisposable
    {
        void Store(StoredEvent theEvent);
        IList<StoredEvent> All(Guid aggregateId);
    }
}
