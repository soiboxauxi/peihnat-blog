﻿using System.Data;

namespace PEIHNAT.Infrastructure.Data
{
    public interface ISqlConnectionFactory
    {
        IDbConnection GetOpenConnection();
    }
}
