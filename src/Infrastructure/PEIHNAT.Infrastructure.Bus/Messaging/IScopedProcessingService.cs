﻿using System.Threading;
using System.Threading.Tasks;

namespace PEIHNAT.Infrastructure.Bus.Messaging
{
    public interface IScopedProcessingService
    {
        Task DoWork(CancellationToken stoppingToken);
    }
}
