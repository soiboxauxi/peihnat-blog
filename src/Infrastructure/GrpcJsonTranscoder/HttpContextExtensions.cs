﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using GrpcJsonTranscoder.Grpc;
using GrpcJsonTranscoder.Internal.Grpc;
using GrpcJsonTranscoder.Internal.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Ocelot.LoadBalancer.LoadBalancers;
using Ocelot.Logging;
using Ocelot.Middleware;
using Ocelot.Responses;
using Ocelot.Configuration;

namespace GrpcJsonTranscoder
{
    public static class HttpContextExtensions
    {
        public static async Task HandleGrpcRequestAsync(
            this HttpContext context,
            Func<Task> next,
            IEnumerable<Interceptor> interceptors = null,
            SslCredentials secureCredentials = null)
        {
            // ignore if the request is not a gRPC content type
            if (!context.Request.Headers.Any(h => h.Key.ToLowerInvariant() == "content-type" && h.Value == "application/grpc"))
            {
                await next.Invoke();
            }
            else
            {
                var methodPath = context.Items.DownstreamRoute().DownstreamPathTemplate.Value;
                var grpcAssemblyResolver = context.RequestServices.GetService<GrpcAssemblyResolver>();
                var methodDescriptor = grpcAssemblyResolver.FindMethodDescriptor(methodPath.Split('/').Last().ToUpperInvariant());
                
                if (methodDescriptor == null)
                {
                    await next.Invoke();
                }
                else
                {
                    var logger = context.RequestServices.GetService<IOcelotLoggerFactory>().CreateLogger<GrpcAssemblyResolver>();
                    var upstreamHeaders = new Dictionary<string, string>
                            {
                                { "x-grpc-route-data", JsonConvert.SerializeObject(context.Items.DownstreamRouteHolder().TemplatePlaceholderNameAndValues.Select(x => new {x.Name, x.Value})) },
                                { "x-grpc-body-data", await context.Items.DownstreamRequest().Content.ReadAsStringAsync() }
                            };
                
                    logger.LogInformation($"Upstream request method is {context.Request.Method}");
                    logger.LogInformation($"Upstream header data for x-grpc-route-data is {upstreamHeaders["x-grpc-route-data"]}");
                    logger.LogInformation($"Upstream header data for x-grpc-body-data is {upstreamHeaders["x-grpc-body-data"]}");
                    var requestObject = context.ParseRequestData(upstreamHeaders);
                    var requestJsonData = JsonConvert.SerializeObject(requestObject);
                    logger.LogInformation($"Request object data is {requestJsonData}");
                
                    var loadBalancerFactory = context.RequestServices.GetService<ILoadBalancerFactory>();
                    var loadBalancerResponse = loadBalancerFactory.Get(
                        context.Items.DownstreamRoute(), 
                        context.Items.IInternalConfiguration().ServiceProviderConfiguration);
                    var serviceHostPort = await loadBalancerResponse.Data.Lease(context);
                
                    var downstreamHost = $"{serviceHostPort.Data.DownstreamHost}:{serviceHostPort.Data.DownstreamPort}";
                    logger.LogInformation($"Downstream IP Address is {downstreamHost}");
                
                    var channel = new Channel(downstreamHost, secureCredentials ?? ChannelCredentials.Insecure);
                
                    MethodDescriptorCaller client = null;
                
                    if (interceptors != null && interceptors.Count() > 0)
                    {
                        CallInvoker callInvoker = null;
                
                        foreach (var inteceptor in interceptors)
                        {
                            callInvoker = channel.Intercept(inteceptor);
                        }
                
                        client = new MethodDescriptorCaller(callInvoker);
                    }
                    else
                    {
                        client = new MethodDescriptorCaller(channel);
                    }
                
                    var concreteObject = JsonConvert.DeserializeObject(requestJsonData, methodDescriptor.InputType.ClrType);
                    var result = await client.InvokeAsync(methodDescriptor, context.GetRequestHeaders(), concreteObject);
                    logger.LogDebug($"gRPC response called with {JsonConvert.SerializeObject(result)}");
                
                    var jsonSerializer = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
                    var response = new OkResponse<GrpcHttpContent>(new GrpcHttpContent(JsonConvert.SerializeObject(result, jsonSerializer)));
                
                    var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = response.Data
                    };
                
                    context.Response.ContentType = "application/json";
                    //context.DownstreamResponse = new DownstreamResponse(httpResponseMessage);
                }
            }
        }
    }
}
