﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using PEIHNAT.Domain.Core.Bus;
using PEIHNAT.Domain.Core.Events;
using PEIHNAT.Domain.Core.Notifications;
using PEIHNAT.Domain.Interfaces;
using PEIHNAT.Infrastructure.CrossCutting.Bus;
using PEIHNAT.Infrastructure.CrossCutting.Identity.Models.AspNet;
using PEIHNAT.Infrastructure.CrossCutting.Identity.Services;
using PEIHNAT.Infrastructure.Data.EventSourcing;
using PEIHNAT.Infrastructure.Data.Repository.EventSourcing;

namespace PEIHNAT.Infrastructure.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();

            // Domain - Events
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();

            // Infra - Data EventSourcing
            services.AddScoped<IEventStoreRepository, EventStoreSQLRepository>();
            services.AddScoped<IEventStore, SqlEventStore>();

            // Infra - Identity
            services.AddScoped<IUser, AspNetUser>();
            services.AddSingleton<IJwtFactory, JwtFactory>();
        }
    }
}
