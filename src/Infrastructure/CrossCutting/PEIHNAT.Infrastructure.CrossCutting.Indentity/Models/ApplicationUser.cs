﻿using Microsoft.AspNetCore.Identity;

namespace PEIHNAT.Infrastructure.CrossCutting.Identity.Models
{
    /// <summary>
    /// ApplicationUser
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
    }
}
