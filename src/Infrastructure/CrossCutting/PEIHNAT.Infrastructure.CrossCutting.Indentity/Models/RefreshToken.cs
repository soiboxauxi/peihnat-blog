﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PEIHNAT.Infrastructure.CrossCutting.Identity.Models
{
    /// <summary>
    /// RefreshToken
    /// </summary>
    public class RefreshToken
    {
        /// <summary>
        /// Token
        /// </summary>
        [Key]
        public string Token { get; set; }

        /// <summary>
        /// JwtId
        /// </summary>
        public string JwtId { get; set; }

        /// <summary>
        /// CreationDate
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// ExpiryDate
        /// </summary>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Used
        /// </summary>
        public bool Used { get; set; }

        /// <summary>
        /// Invalidated
        /// </summary>
        public bool Invalidated { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }
    }
}
