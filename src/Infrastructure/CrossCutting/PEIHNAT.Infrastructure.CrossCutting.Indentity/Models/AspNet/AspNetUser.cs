﻿using Microsoft.AspNetCore.Http;
using PEIHNAT.Domain.Interfaces;
using System.Collections.Generic;
using System.Security.Claims;

namespace PEIHNAT.Infrastructure.CrossCutting.Identity.Models.AspNet
{
    public class AspNetUser : IUser
    {
        private readonly IHttpContextAccessor _accessor;

        public AspNetUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public string Name => _accessor.HttpContext.User.Identity.Name;

        public bool IsAuthenticated()
        {
            return _accessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public IEnumerable<Claim> GetClaimsIdentity()
        {
            return _accessor.HttpContext.User.Claims;
        }
    }
}
