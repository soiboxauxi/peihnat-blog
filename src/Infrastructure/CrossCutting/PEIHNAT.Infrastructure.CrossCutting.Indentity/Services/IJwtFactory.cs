﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace PEIHNAT.Infrastructure.CrossCutting.Identity.Services
{
    public interface IJwtFactory
    {
        Task<JwtToken> GenerateJwtToken(ClaimsIdentity claimsIdentity);
    }
}
