﻿using DbUp;
using DbUp.Engine;
using Microsoft.Extensions.Configuration;
using PEIHNAT.DbMigration.Consts;
using System;
using System.Threading.Tasks;

namespace PEIHNAT.DbMigration
{
    class Program
    {
        private static IConfiguration _configuration;

        static async Task Main(string[] args)
        {
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            for (var argIndex = 0; argIndex < args.Length; argIndex++)
            {
                switch (args[argIndex])
                {
                    case ServiceConsts.AspNet:
                        {
                            Run(ServiceConsts.AspNet);
                            break;
                        }
                    case ServiceConsts.Posts:
                        {
                            Run(ServiceConsts.Posts);
                            break;
                        }
                    case ServiceConsts.Search:
                        {
                            await SeedSearchDbAsync(ServiceConsts.Search);
                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException($"{args[argIndex]} not found.");
                }
            }
        }

        private static void Run(string serviceName)
        {
            string connString = _configuration.GetConnectionString(serviceName);
            string scriptFolderPath = $"./Scripts/{serviceName}";

            DropDatabase.For.SqlDatabase(connString);
            EnsureDatabase.For.SqlDatabase(connString);

            var upgrader = DeployChanges.To
                .SqlDatabase(connString, null)
                .WithScriptsFromFileSystem(scriptFolderPath, new SqlScriptOptions
                {
                    RunGroupOrder = DbUpDefaults.DefaultRunGroupOrder + 1
                })
                .LogToConsole()
                .Build();

            upgrader.PerformUpgrade();
        }

        private static async Task SeedSearchDbAsync(string serviceName)
        {
            return;
        }
    }
}
