﻿IF SCHEMA_ID(N'Posts') IS NULL EXEC(N'CREATE SCHEMA [Posts];');
GO
	/****** Object:  Table [dbo].[Post] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO IF NOT EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[Posts]')
			AND type in (N'U')
	) BEGIN CREATE TABLE [dbo].[Posts](
		[Id] [uniqueidentifier] NOT NULL,
		[AuthorId] [bigint] NOT NULL,
		[ParentId] [bigint] NULL,
		[Title] [nvarchar](256) NULL,
		[MetaTitle] [nvarchar](256) NULL,
		[Summary] [text] NULL,
		[Published] [bit] NOT NULL,
		[CreatedAt] [datetime2(7)] NOT NULL,
		[UpdatedAt] [datetime2(7)] NOT NULL,
		[PublishedAt] [datetime2(7)] NOT NULL,
		[Text] [text] NULL,
		CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
			PAD_INDEX = OFF,
			STATISTICS_NORECOMPUTE = OFF,
			IGNORE_DUP_KEY = OFF,
			ALLOW_ROW_LOCKS = ON,
			ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO