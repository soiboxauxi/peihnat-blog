﻿using PEIHNAT.Domain.Core.Commands;
using PEIHNAT.Domain.Core.Events;
using System.Threading.Tasks;

namespace PEIHNAT.Domain.Core.Bus
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T command) where T : Command;
        Task RaiseEvent<T>(T @event) where T : Event;
    }
}
