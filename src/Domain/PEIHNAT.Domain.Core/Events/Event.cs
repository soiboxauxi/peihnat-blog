﻿using MediatR;
using PEIHNAT.Domain.Core.Messages;
using System;

namespace PEIHNAT.Domain.Core.Events
{
    public abstract class Event : Message, INotification
    {
        public DateTime Timestamp { get; private set; }

        protected Event()
        {
            Timestamp = DateTime.Now;
        }
    }
}
