﻿using System;
using System.ComponentModel.DataAnnotations;
using static PEIHNAT.Utils.Helpers.DateTimeHelper;

namespace PEIHNAT.Domain
{
    public interface IEntity<TId> : IIdentity<TId> { }

    /// <inheritdoc />
    /// <summary>
    ///  Source: https://github.com/VaughnVernon/IDDD_Samples_NET
    /// </summary>
    public abstract class EntityBase<TId> : IEntity<TId>
    {
        protected EntityBase(TId id)
        {
            Id = id;
            CreatedAt = NewDateTime();
        }

        public DateTime CreatedAt { get; protected set; }

        public DateTime? UpdatedAt { get; protected set; }

        [Key] 
        public TId Id { get; protected set; }
    }
}
