﻿using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PEIHNAT.Infrastructure;
using PEIHNAT.Infrastructure.Bus;
using PEIHNAT.Infrastructure.Bus.InterProc.Redis;
using PEIHNAT.Infrastructure.Bus.Messaging;
using PEIHNAT.Infrastructure.Data;
using PEIHNAT.Infrastructure.Data.Dapper.Core;
using PEIHNAT.Infrastructure.Data.EfCore;
using PEIHNAT.Infrastructure.Data.EfCore.Core;
using PEIHNAT.PostService.Domain;
using PEIHNAT.PostService.ProcessingServices;
using System.Reflection;

namespace PEIHNAT.PostService
{
    public static class Startup
    {
        public static IServiceCollection AddServiceComponents(
            this IServiceCollection services, IConfiguration config)
        {
            var dbOptions = new EfDbOptions();
            config.Bind("ConnectionStrings", dbOptions);

            services.AddDbContext<MessagingDataContext>(options => options.UseSqlServer(dbOptions.MainDb));
            services.AddScoped<IEfUnitOfWork<MessagingDataContext>, EfUnitOfWork<MessagingDataContext>>();

            services.Configure<DapperDbOptions>(config.GetSection("ConnectionStrings"));
            services.AddScoped<ISqlConnectionFactory, SqlConnectionFactory>();
            services.AddScoped<IDapperUnitOfWork, DapperUnitOfWork>();

            services.AddMediatR(Assembly.GetEntryAssembly(), typeof(Startup).Assembly);
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddServiceByInterfaceInAssembly<Post>(typeof(IValidator<>));
            //services.AddScoped<IDomainEventDispatcher, DomainEventDispatcher<MessagingDataContext>>();

            services.Configure<RedisOptions>(config.GetSection("Redis"));
            services.AddScoped<RedisStore>();
            services.AddScoped<IMessageBus, RedisMessageBus>();

            services.AddScoped<IScopedProcessingService, ScopedProcessingService>();
            return services;
        }
    }
}
