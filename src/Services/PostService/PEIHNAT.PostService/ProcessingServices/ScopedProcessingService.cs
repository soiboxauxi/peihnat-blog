﻿using Microsoft.Extensions.Logging;
using PEIHNAT.Infrastructure.Bus;
using PEIHNAT.Infrastructure.Bus.Messaging;
using PEIHNAT.Infrastructure.Data.EfCore.Core;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace PEIHNAT.PostService.ProcessingServices
{
    public class ScopedProcessingService : OutboxScopedProcessingServiceBase, IScopedProcessingService
    {
        public ScopedProcessingService(
            IEfUnitOfWork<MessagingDataContext> unitOfWork, 
            IMessageBus messageBus, 
            ILogger<ScopedProcessingService> logger)
            : base(unitOfWork, messageBus, logger) { }

        public async Task DoWork(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                Logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await PublishEventsUnProcessInOutboxToChannels("product_catalog_service");
                await Task.Delay(5000, stoppingToken);
            }
        }

        public override bool ScanAssemblyWithConditions(Assembly assembly)
        {
            return assembly.GetName().Name.Contains("PEIHNAT.PostService.DataContracts");
        }
    }
}
