﻿using MediatR;
using PEIHNAT.Infrastructure.Data.Dapper.Core;
using PEIHNAT.PostService.DataContracts.Api.V1;
using PEIHNAT.PostService.Domain;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PEIHNAT.PostService.Usecases.GetPosts
{
    public class GetPostsHandler : IRequestHandler<GetPostRequest, GetPostResponse>
    {
        private readonly IDapperUnitOfWork _unitOfWork;

        public GetPostsHandler(IDapperUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<GetPostResponse> Handle(GetPostRequest request, CancellationToken cancellationToken)
        {
            var postRepository = _unitOfWork.QueryRepository<Post, Guid>();
            var queryable = postRepository.Queryable();
            var posts = queryable
                //.Skip(request.CurrentPage - 1)
                //.Take(10)
                //.Where(x => !x.IsDeleted && x.Price <= request.HighPrice)
                .ToList();

            var response = new GetPostResponse();
            response.Posts
                .AddRange(posts.Select(x => 
                {
                    return new DataContracts.Dto.V1.PostDto
                    {
                        Id = x.Id.ToString(),
                    };
                }));

            return await Task.FromResult(response);
        }
    }
}
