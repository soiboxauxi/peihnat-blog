﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PEIHNAT.PostService.Api.Options
{
    public class PostServiceOptions
    {
        /// <summary>
        /// Gets the major version number.
        /// </summary>
        public int DefaultMajorVersion { get; set; }

        /// <summary>
        /// Gets the minor version number.
        /// </summary>
        public int DefaultMinorVersion { get; set; }
    }
}
