﻿using CloudNativeKit.Infrastructure.SysInfo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static CloudNativeKit.Infrastructure.SysInfo.ConfigurationExtensions;

namespace PEIHNAT.PostService.Api.Controllers
{
    /// <summary>
    /// InfoController
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class InfoController : ControllerBase
    {
        private readonly IConfiguration _config;

        /// <summary>
        /// InfoController
        /// </summary>
        /// <param name="config"></param>
        public InfoController(IConfiguration config)
        {
            _config = config;
        }

        /// <summary>
        /// Info
        /// </summary>
        /// <returns></returns>
        [HttpGet("/info")]
        public ActionResult<SysInfoModel> Info()
        {
            return new JsonResult(_config.GetSystemInformation());
        }
    }
}
