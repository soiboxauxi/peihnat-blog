﻿using Microsoft.AspNetCore.Authorization;
using PEIHNAT.PostService.DataContracts.Api.V1;
using MediatR;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using PEIHNAT.PostService.DataContracts.Dto.V1;
using Google.Protobuf.Collections;

namespace PEIHNAT.PostService.Api.GrpcServices
{
    /// <summary>
    /// PostService
    /// </summary>
    [Authorize]
    public class PostService : PostApi.PostApiBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<PostService> _logger;
        /// <summary>
        /// PostService
        /// </summary>
        /// <param name="mediator"></param>
        public PostService(
            IMediator mediator,
            ILogger<PostService> logger
            )
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// GetPost
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task<GetPostResponse> GetPost(
            GetPostRequest request, ServerCallContext context)
        {
            var user = context.GetHttpContext().User;
            //return Task.FromResult(new GetPostResponse
            //{
            //    Posts = { new PostDto { Id = "1" } }
            //});
            return await _mediator.Send(request);
        }
    }    
}
