﻿using System.ComponentModel;
using MediatR;

namespace PEIHNAT.PostService.DataContracts.Api.V1
{
    [DefaultValue("DefaultReflection")]
    public static partial class ApiPartials { }

    public partial class GetPostRequest : IRequest<GetPostResponse> { }
}
