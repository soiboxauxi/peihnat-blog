﻿using AutoMapper;
using IdentityServer.Core.Databases;
using IdentityServer.Core.Interfaces;
using IdentityServer.Core.Mapping;
using IdentityServer.Core.Models;
using IdentityServer.Core.Permissions;
using IdentityServer.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace IdentityServer.Core
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// AddRegisterAuth
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public static IServiceCollection AddRegisterAuth<TDbContext>(
            this IServiceCollection service,
            Action<DbContextOptionsBuilder> optionsAction = null)
            where TDbContext : BaseAuthDbContext
        {
            service.AddTransient<ICheckPermissionService, DbCheckPermissionService>();

            service.AddDbContext<TDbContext>(optionsAction);
            service.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
               .AddEntityFrameworkStores<TDbContext>()
               .AddDefaultTokenProviders();

            service.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();

            service.AddTransient<IUserService, UserService>();
            service.AddAutoMapper(typeof(AutoMapperProfile).Assembly);

            return service;
        }
    }
}
