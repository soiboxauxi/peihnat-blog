﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityServer.Core.Models
{
    [Table("ApplicationRoles")]
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
            : base()
        {

        }

        public ApplicationRole(string roleName)
            : base(roleName)
        {

        }
    }
}
