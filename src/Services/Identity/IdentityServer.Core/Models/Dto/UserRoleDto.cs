﻿namespace IdentityServer.Core.Models.Dto
{
    public class UserRoleDto
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool Selected { get; set; }
    }
}
