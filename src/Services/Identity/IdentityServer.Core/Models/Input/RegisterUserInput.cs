﻿using IdentityServer.Core.Models.Dto;
using System.Collections.Generic;

namespace IdentityServer.Core.Models.Input
{
    public class RegisterUserInput
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public List<UserRoleDto> Roles { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
