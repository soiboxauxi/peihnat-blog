﻿using IdentityServer.Core.Models.Dto;
using System.Collections.Generic;

namespace IdentityServer.Core.Models.Input
{
    public class EditUserInput
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string NormalizedUserName { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public List<UserRoleDto> Roles { get; set; }
    }
}
