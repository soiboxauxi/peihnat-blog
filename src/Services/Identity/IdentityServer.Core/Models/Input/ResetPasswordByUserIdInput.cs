﻿namespace IdentityServer.Core.Models.Input
{
    public class ResetPasswordByUserIdInput
    {
        public string UserId { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
