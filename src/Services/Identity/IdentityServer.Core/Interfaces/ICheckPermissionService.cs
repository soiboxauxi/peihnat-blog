﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Core.Interfaces
{
    public interface ICheckPermissionService
    {
        Task<bool> HasPermission(IEnumerable<Claim> Claims, string permission);
    }
}
