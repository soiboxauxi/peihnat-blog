﻿using IdentityServer.Core.Models.Input;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace IdentityServer.Core.Interfaces
{
    public interface IUserService
    {
        Task<EditUserInput> GetById(string userId);
        Task Delete(string userId);
        Task Update(EditUserInput input);
        Task<IdentityResult> Register(RegisterUserInput input);
        Task ChangePassword(ChangePasswordInput input);
        Task ResetPasswordAsync(ResetPasswordByUserIdInput input);
    }
}
