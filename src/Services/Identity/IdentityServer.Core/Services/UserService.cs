﻿using AutoMapper;
using IdentityServer.Core.Databases.EntityFrameworkCore;
using IdentityServer.Core.Interfaces;
using IdentityServer.Core.Models;
using IdentityServer.Core.Models.Input;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Core.Services
{
    class UserService : IUserService
    {
        private readonly AuthDbContext _authDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(
            UserManager<ApplicationUser> userManager, 
            RoleManager<ApplicationRole> roleManager, 
            IMapper mapper, 
            AuthDbContext authDbContext, 
            IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _authDbContext = authDbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            _httpContextAccessor = httpContextAccessor;
        }
        public Task ChangePassword(ChangePasswordInput input)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IdentityResult> Register(RegisterUserInput input)
        {
            var user = new ApplicationUser();
            var applicationUser = _mapper.Map(input, user);
            applicationUser.EmailConfirmed = true;
            var result = await _userManager.CreateAsync(user, input.Password);
            if (!result.Succeeded)
            {
                throw new Exception(string.Join("", result.Errors.Select(e => MappErrorCode(e.Code))));
            }

            await UpdateUserRole(user.Id, 
                input.Roles.Where(x => x.Selected).Select(x => x.RoleName).ToList());

            return result;
        }

        public Task Delete(string userId)
        {
            throw new System.NotImplementedException();
        }

        public Task<EditUserInput> GetById(string userId)
        {
            throw new System.NotImplementedException();
        }

        public Task ResetPasswordAsync(ResetPasswordByUserIdInput input)
        {
            throw new System.NotImplementedException();
        }

        public Task Update(EditUserInput input)
        {
            throw new System.NotImplementedException();
        }

        private async Task UpdateUserRole(string userId, List<string> selectedRoles)
        {
            var user = await _userManager.FindByIdAsync(userId);
            IList<string> roles = await _userManager.GetRolesAsync(user);
            IEnumerable<string> deleteRoles = roles.Where(c => !selectedRoles.Contains(c));
            if (deleteRoles.ToList().Count > 0)
            {
                await _userManager.RemoveFromRolesAsync(user, deleteRoles);
            }
            IEnumerable<string> newRoles = selectedRoles.Where(c => !roles.Contains(c));

            if (newRoles.ToList().Count > 0)
            {
                await _userManager.AddToRolesAsync(user, newRoles);
            }
        }

        private string MappErrorCode(string Code)
        {
            switch (Code)
            {
                case "PasswordTooShort":
                    return "Mật khẩu cần ít nhất 6 kí tự.";
                case "PasswordRequiresNonAlphanumeric":
                    return "Mật khẩu cần có ít nhất 1 một ký tự đặc biệt.";
                case "PasswordRequiresUpper":
                    return "Mật khẩu cần có it nhất 1 chữ hoa.";
                case "PasswordRequiresLower":
                    return "Mật khẩu cần có it nhất 1 chữ thường.";
                default:
                    break;
            }
            return Code;
        }
    }
}
