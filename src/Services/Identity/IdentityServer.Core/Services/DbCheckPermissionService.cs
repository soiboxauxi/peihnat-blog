﻿using IdentityServer.Core.Interfaces;
using IdentityServer.Core.Models;
using IdentityServer.Core.Types;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Core.Services
{
    public class DbCheckPermissionService : ICheckPermissionService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public DbCheckPermissionService(
            UserManager<ApplicationUser> userManager, 
            RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<bool> HasPermission(IEnumerable<Claim> claims, string permission)
        {
            var sub = claims.FirstOrDefault(claim => claim.Type == "sub");
            if (sub == null)
            {
                return false;
            }
            var userId = sub.Value;
            if (string.IsNullOrEmpty(userId))
            {
                return false;
            }
            var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
            var userRoleNames = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            var userRoles = _roleManager.Roles.Where(x => userRoleNames.Contains(x.Name));

            foreach (var role in userRoles)
            {
                var roleClaims = await _roleManager.GetClaimsAsync(role).ConfigureAwait(false);
                var hasPermission = roleClaims.Where(x => x.Type == CustomClaimTypes.Permission &&
                                                        x.Value == permission &&
                                                        x.Issuer == "LOCAL AUTHORITY")
                                            .Any();

                if (hasPermission)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
