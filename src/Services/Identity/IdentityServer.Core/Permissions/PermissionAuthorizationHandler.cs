﻿using IdentityServer.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace IdentityServer.Core.Permissions
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        private readonly ICheckPermissionService _checkPermissionService;

        public PermissionAuthorizationHandler(ICheckPermissionService checkPermissionService)
        {
            _checkPermissionService = checkPermissionService;
        }

        protected override async Task HandleRequirementAsync(
            AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (requirement == null)
            {
                throw new System.ArgumentNullException(nameof(requirement));
            }
            if (context?.User == null)
            {
                return;
            }

            var hasPermission = await _checkPermissionService.HasPermission(context.User.Claims, requirement.Permission).ConfigureAwait(false);
            if (hasPermission)
            {
                context.Succeed(requirement);
            }
        }
    }
}
