﻿using IdentityServer.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Core.Databases
{
    public class BaseAuthDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public BaseAuthDbContext(DbContextOptions options) : base(options)
        {
        
        }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            if (builder == null)
            {
                throw new System.ArgumentNullException(nameof(builder));
            }
            base.OnModelCreating(builder);
            
            builder.Entity<ApplicationUser>().ToTable("ApplicationUsers");
            builder.Entity<IdentityUserRole<string>>().ToTable("IdentityUserRoles");
            builder.Entity<IdentityUserLogin<string>>().ToTable("IdentityUserLogins");
            builder.Entity<IdentityUserClaim<string>>().ToTable("IdentityUserClaims");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("IdentityRoleClaims");
            builder.Entity<IdentityUserToken<string>>().ToTable("IdentityUserTokens");
            builder.Entity<ApplicationRole>().ToTable("ApplicationRoles");
        }
    }
}
