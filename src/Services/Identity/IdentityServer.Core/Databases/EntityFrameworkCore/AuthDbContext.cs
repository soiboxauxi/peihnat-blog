﻿using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Core.Databases.EntityFrameworkCore
{
    public class AuthDbContext : BaseAuthDbContext
    {
        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options)
        {

        }
    }
}
