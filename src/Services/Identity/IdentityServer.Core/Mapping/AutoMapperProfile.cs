﻿using AutoMapper;
using IdentityServer.Core.Models;
using IdentityServer.Core.Models.Input;

namespace IdentityServer.Core.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RegisterUserInput, ApplicationUser>().ReverseMap();
        }
    }
}
