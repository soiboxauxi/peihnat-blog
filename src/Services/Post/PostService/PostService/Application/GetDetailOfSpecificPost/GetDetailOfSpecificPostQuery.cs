﻿using FluentValidation;
using MediatR;
using N5T.Infrastructure.Auth;
using NT5.Infrastructure.App.Dtos;
using System;

namespace PostService.Application.GetDetailOfSpecificPost
{
    public class GetDetailOfSpecificPostQuery : IRequest<FlatPostDto>, IAuthRequest
    {
        public Guid Id { get; set; }
    }

    public class GetDetailOfSpecificPostValidator : AbstractValidator<GetDetailOfSpecificPostQuery>
    {
        public GetDetailOfSpecificPostValidator()
        {
        }
    }
}
