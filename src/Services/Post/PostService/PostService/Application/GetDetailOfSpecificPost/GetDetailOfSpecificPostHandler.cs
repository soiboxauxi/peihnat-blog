﻿using MediatR;
using NT5.Infrastructure.App.Dtos;
using System.Threading;
using System.Threading.Tasks;

namespace PostService.Application.GetDetailOfSpecificPost
{
    public class GetDetailOfSpecificPostHandler : IRequestHandler<GetDetailOfSpecificPostQuery, FlatPostDto>
    {
        //private readonly IDbContextFactory<MainDbContext> _dbContextFactory;
        //private readonly IInventoryGateway _inventoryGateway;

        public GetDetailOfSpecificPostHandler(
            //IDbContextFactory<MainDbContext> dbContextFactory,
            //IInventoryGateway inventoryGateway
            )
        {
            //_dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
            //_inventoryGateway = inventoryGateway ?? throw new ArgumentNullException(nameof(inventoryGateway));
        }

        public Task<FlatPostDto> Handle(GetDetailOfSpecificPostQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult<FlatPostDto>(new FlatPostDto { Id = request.Id, Content = "Content 1" });//This will compile;
        }
    }
}
