using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using N5T.Infrastructure;
using N5T.Infrastructure.Auth;
using N5T.Infrastructure.Logging;
using N5T.Infrastructure.Tye;
using N5T.Infrastructure.Validator;

namespace PostService.Api
{
    public class Startup
    {
        public Startup(IConfiguration config, IWebHostEnvironment env)
        {
            Config = config;
            Env = env;
        }

        private IConfiguration Config { get; }
        private IWebHostEnvironment Env { get; }
        private bool IsRunOnTye => Config.IsRunOnTye();

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor()
                .AddCustomMediatR<Anchor>()
                .AddCustomValidators<Anchor>()
                .AddControllers();

            services.AddCustomAuth<Anchor>(Config, options =>
            {
                options.Authority = IsRunOnTye
                    ? Config.GetServiceUri("identityapp")?.AbsoluteUri
                    : options.Authority;

                options.Audience = IsRunOnTye
                    ? $"{Config.GetServiceUri("identityapp")?.AbsoluteUri.TrimEnd('/')}/resources"
                    : options.Audience;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.ApplicationServices.CreateLoggerConfiguration(IsRunOnTye);
        }
    }
}
