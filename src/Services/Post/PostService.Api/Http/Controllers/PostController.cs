﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NT5.Infrastructure.App.Dtos;
using PostService.Application.GetDetailOfSpecificPost;
using System;
using System.Threading.Tasks;

namespace PostService.Api.Http.Controllers
{
    [ApiController]
    [Route("api/posts")]
    public class PostController : ControllerBase
    {
        [Authorize]
        [HttpGet("{id}")]
        public async Task<FlatPostDto> Get([FromServices] IMediator mediator, Guid id) =>
            await mediator.Send(new GetDetailOfSpecificPostQuery { Id = id });
    }
}
