﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using N5T.Infrastructure.Status;

namespace PostService.Api.Http.Controllers
{
    [ApiController]
    [Route("")]
    public class InfoController : ControllerBase
    {
        [HttpGet("/info")]
        public IActionResult Status([FromServices] IConfiguration config) => Content(config.BuildAppStatus());
    }
}
