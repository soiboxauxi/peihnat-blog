﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PEIHNAT.Infrastructure.CrossCutting.Identity.Database;
using PEIHNAT.Infrastructure.CrossCutting.Identity.Models;
using PEIHNAT.Infrastructure.CrossCutting.Identity.Options;
using System.Text;

namespace PEIHNAT.AccessControl.Extensions
{
    /// <summary>
    /// AddCustomizedAuth
    /// </summary>
    public static class AuthExtension
    {
        /// <summary>
        /// AddCustomizedAuth
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomizedAuth(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            // Get settings
            //string secretKey = configuration.GetValue<string>("SecretKey");
            string secretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH";


            var jwtAppSettingOptions = configuration.GetSection(nameof(JwtIssuerOptions));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AuthDbContext>()
                .AddDefaultTokenProviders();

            SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            return services;
        }
    }
}
