﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PEIHNAT.AccessControl.Options
{
    /// <summary>
    /// AccessControlOptions
    /// </summary>
    public class AccessControlOptions
    {
        /// <summary>
        /// Gets the major version number.
        /// </summary>
        public int DefaultMajorVersion { get; set; }

        /// <summary>
        /// Gets the minor version number.
        /// </summary>
        public int DefaultMinorVersion { get; set; }
    }
}
