using Extensions.Swagger;
using Extensions.Swagger.Options;
using Extensions.WebApplication;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PEIHNAT.AccessControl.Extensions;
using PEIHNAT.AccessControl.Options;
using PEIHNAT.Infrastructure.CrossCutting.IoC;
using IdentityServer4;
using Microsoft.IdentityModel.Logging;

namespace PEIHNAT.AccessControl
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Configuration
        /// </summary>
        public IConfiguration Configuration { get; }
        private readonly IWebHostEnvironment _env;

        /// <summary>
        /// Startup
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="env"></param>
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
            IdentityModelEventSource.ShowPII = true;
        }

        /// <summary>
        /// ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        /// This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);

            SwaggerOptions swaggerOptions = Configuration.GetOption<SwaggerOptions>();
            services.AddSwagger(
                options =>
                {
                    options.Name = swaggerOptions.Name;
                    options.IsBearerAuthorizationRequired = swaggerOptions.IsBearerAuthorizationRequired;
                    options.OpenApiInfo = swaggerOptions.OpenApiInfo;
                    options.XmlDocumentationPath = swaggerOptions.XmlDocumentationPath;
                }
            );

            AccessControlOptions accessControlOptions = Configuration.GetOption<AccessControlOptions>();
            services.AddApiVersioning
                    (
                        config =>
                        {
                            config.DefaultApiVersion = new ApiVersion(
                                accessControlOptions.DefaultMajorVersion,
                                accessControlOptions.DefaultMinorVersion);
                            config.AssumeDefaultVersionWhenUnspecified = true;
                            config.ReportApiVersions = true;
                            config.ApiVersionReader = new UrlSegmentApiVersionReader();
                        }
                    );

            services.AddVersionedApiExplorer();

            // ----- Database -----
            services.AddCustomizedDatabase(Configuration, _env);

            // ----- Auth -----
            services.AddCustomizedAuth(Configuration);

            var builder = services.AddIdentityServer(
                options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                    options.EmitStaticAudienceClaim = true;
                }
                )
                //.AddTestUsers(TestUsers.Users)
                //.AddProfileService<CustomizedProfileService>()
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApis())
                .AddInMemoryApiScopes(Config.GetScopes())
                .AddInMemoryClients(Config.GetClients());

            builder.AddDeveloperSigningCredential();

            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    // register your IdentityServer with Google at https://console.developers.google.com
                    // enable the Google+ API
                    // set the redirect URI to https://localhost:5001/signin-google
                    options.ClientId = "copy client ID from Google here";
                    options.ClientSecret = "copy client secret from Google here";
                });

            // Adding MediatR for Domain Events and Notifications
            services.AddMediatR(typeof(Startup));

            // .NET Native DI Abstraction
            RegisterServices(services);

            services.AddControllers();

        }

        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="provider"></param>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            // ----- CORS -----
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseCustomizedSwagger(provider);

            app.UseIdentityServer();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void RegisterServices(IServiceCollection services)
        {
            // Adding dependencies from another layers (isolated from Presentation)
            NativeInjectorBootStrapper.RegisterServices(services);
        }
    }
}
