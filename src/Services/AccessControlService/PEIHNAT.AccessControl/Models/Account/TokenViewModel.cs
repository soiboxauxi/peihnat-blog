﻿namespace PEIHNAT.AccessControl.Models.Account
{
    /// <summary>
    /// TokenViewModel
    /// </summary>
    public class TokenViewModel
    {
        /// <summary>
        /// AccessToken
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// RefreshToken
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
