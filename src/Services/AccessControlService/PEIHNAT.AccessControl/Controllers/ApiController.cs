﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PEIHNAT.Domain.Core.Bus;
using PEIHNAT.Domain.Core.Notifications;
using System.Linq;

namespace PEIHNAT.AccessControl.Controllers
{
    /// <summary>
    /// ApiController
    /// </summary>
    public class ApiController : ControllerBase
    {
        private readonly DomainNotificationHandler _notifications;
        private readonly IMediatorHandler _mediator;

        /// <summary>
        /// ApiController
        /// </summary>
        /// <param name="notifications"></param>
        /// <param name="mediator"></param>
        public ApiController(
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator)
        {
            _notifications = (DomainNotificationHandler)notifications;
            _mediator = mediator;
        }

        /// <summary>
        /// IsValidOperation
        /// </summary>
        /// <returns></returns>
        protected bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }

        /// <summary>
        /// Response
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        protected new IActionResult Response(object result = null)
        {
            if (IsValidOperation())
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new
            {
                success = false,
                errors = _notifications.GetNotifications().Select(n => n.Value)
            });
        }

        /// <summary>
        /// NotifyModelStateErrors
        /// </summary>
        protected void NotifyModelStateErrors()
        {
            var erros = ModelState.Values.SelectMany(v => v.Errors);
            foreach (var erro in erros)
            {
                var erroMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
                NotifyError(string.Empty, erroMsg);
            }
        }

        /// <summary>
        /// NotifyError
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        protected void NotifyError(string code, string message)
        {
            _mediator.RaiseEvent(new DomainNotification(code, message));
        }

        /// <summary>
        /// AddIdentityErrors
        /// </summary>
        /// <param name="result"></param>
        protected void AddIdentityErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                NotifyError(result.ToString(), error.Description);
            }
        }
    }
}
