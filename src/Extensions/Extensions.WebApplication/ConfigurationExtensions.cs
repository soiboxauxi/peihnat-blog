﻿using Microsoft.Extensions.Configuration;
using System;

namespace Extensions.WebApplication
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Get Option from IConfiguration
        /// </summary>
        /// <typeparam name="TOptions">The Type Option</typeparam>
        /// <param name="configuration">IConfiguration</param>
        /// <returns>Option</returns>
        /// <exception cref="ArgumentNullException">Configuration input is null</exception>
        public static TOptions GetOption<TOptions>(this IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            return configuration.GetSection(typeof(TOptions).Name).Get<TOptions>();
        }
    }
}
