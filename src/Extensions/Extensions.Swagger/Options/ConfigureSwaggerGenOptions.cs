﻿using Extensions.Swagger.Properties;
using Extensions.Swagger.Schemas;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;

namespace Extensions.Swagger.Options
{
    /// <summary>
    /// ConfigureSwaggerOptions
    /// </summary>
    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {
        private readonly IApiVersionDescriptionProvider _provider;
        private readonly SwaggerOptions _swaggerOptions;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="apiVersionDescriptionProvider"></param>
        /// <param name="swaggerOptions"></param>
        public ConfigureSwaggerOptions(IApiVersionDescriptionProvider apiVersionDescriptionProvider, IOptions<SwaggerOptions> swaggerOptions)
        {
            _provider = apiVersionDescriptionProvider ?? throw new ArgumentNullException(nameof(apiVersionDescriptionProvider));
            _swaggerOptions = swaggerOptions?.Value ?? throw new ArgumentNullException(nameof(swaggerOptions));
        }

        /// <summary>
        /// Configure SwaggerGenOptions
        /// </summary>
        /// <param name="options"></param>
        public void Configure(SwaggerGenOptions options)
        {
            if (_swaggerOptions.IsBearerAuthorizationRequired)
            {
                options.AddSecurityDefinition(BearerSchema.BEARER_SCHEME_NAME, BearerSchema.bearerOpenApiSecurityScheme);

                options.AddSecurityRequirement(BearerSchema.bearerOpenApiSecurityRequirement);
            }

            if (!String.IsNullOrWhiteSpace(_swaggerOptions.XmlDocumentationPath))
            {
                options.IncludeXmlComments(_swaggerOptions.XmlDocumentationPath);
            }

            OpenApiInfo defaultApiInfo = _swaggerOptions.OpenApiInfo;

            foreach (var description in _provider.ApiVersionDescriptions)
            {
                options.SwaggerDoc
                (
                    description.GroupName,
                    new OpenApiInfo()
                    {
                        Title = defaultApiInfo.Title,
                        Description = defaultApiInfo.Description,
                        TermsOfService = defaultApiInfo.TermsOfService,
                        Contact = defaultApiInfo.Contact,
                        License = defaultApiInfo.License,
                        Version = description.ApiVersion.ToString(),
                    }
                );
            }
        }
    }
}
