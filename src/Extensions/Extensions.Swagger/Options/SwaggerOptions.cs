﻿using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Extensions.Swagger.Options
{
    /// <summary>
    /// SwaggerOptions
    /// </summary>
    public class SwaggerOptions
    {
        /// <summary>
        /// Document name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// API XML documentation path
        /// </summary>
        public string XmlDocumentationPath { get; set; }

        /// <summary>
        /// RequireBearerAuthorization
        /// </summary>
        public bool IsBearerAuthorizationRequired { get; set; }


        /// <summary>
        /// Document options
        /// </summary>
        public OpenApiInfo OpenApiInfo { get; set; }
    }
}
