﻿using Extensions.Swagger.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;

namespace Extensions.Swagger
{
    /// <summary>
    /// ServiceCollectionExtensions
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// AddSwagger
        /// </summary>
        /// <param name="service"></param>
        /// <param name="configSwaggerOptions"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwagger(
            this IServiceCollection service,
            Action<SwaggerOptions> configSwaggerOptions)
        {
            service.Configure<SwaggerOptions>(options => configSwaggerOptions(options));
                        
            service.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>()
                .AddSwaggerGen();
            
            return service;
        }
    }
}
