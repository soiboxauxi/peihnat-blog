﻿using Extensions.Swagger.Properties;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Extensions.Swagger.Schemas
{
    /// <summary>
    /// BearerSchema
    /// </summary>
    public class BearerSchema
    {
        /// <summary>
        /// SECURITY_NAME
        /// </summary>
        public const string SECURITY_NAME = "Authorization";

        /// <summary>
        /// BEARER_SCHEME_NAME
        /// </summary>
        public const string BEARER_SCHEME_NAME = "Bearer";

        /// <summary>
        /// Add Bearer token in header request
        /// </summary>
        public static readonly OpenApiSecurityScheme bearerOpenApiSecurityScheme = new OpenApiSecurityScheme
        {
            Description = Resources.SwaggerAuthorizationMessage,
            Name = SECURITY_NAME,
            In = ParameterLocation.Header,
            Type = SecuritySchemeType.ApiKey,
            Scheme = BEARER_SCHEME_NAME
        };

        private static readonly OpenApiReference _bearerOpenApiReference = new OpenApiReference
        {
            Type = ReferenceType.SecurityScheme,
            Id = BEARER_SCHEME_NAME
        };

        private static readonly OpenApiSecurityScheme _requireBearerOpenApiSecurityScheme = new OpenApiSecurityScheme
        {
            Reference = _bearerOpenApiReference
        };

        /// <summary>
        /// bearerOpenApiSecurityRequirement
        /// </summary>
        public static readonly OpenApiSecurityRequirement bearerOpenApiSecurityRequirement = new OpenApiSecurityRequirement
        {
            { _requireBearerOpenApiSecurityScheme, new List<string>() }
        };
    }
}
