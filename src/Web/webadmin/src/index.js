import createRoutes from '@/routes';
import { ConfigProvider } from 'antd';
import en_US from 'antd/es/locale/en_US';
import 'assets/styles/index.less';
import dva, { dynamic, router } from 'dva';
import createLoading from 'dva-loading';
import { createHashHistory } from 'history';
import React from 'react';
import { homepage } from '../package.json';
import config from './config';
import reportWebVitals from './reportWebVitals';
import request from 'cmn-utils/lib/request';

const { Router } = router;
require('dotenv').config();

// 1. Initialization
const app = dva({
  history: createHashHistory({
    basename: homepage.startsWith('/') ? homepage : ''
  })
});

// 2. Plug-in
app.use(createLoading());
app.use({ onError: config.exception.global });

// -> request
request.config(config.request);

// Use mock data
require('./__mocks__');
// -> Developer mock data
// if (process.env.NODE_ENV === 'development') {
//   require('./__mocks__');
// }

// -> loading
dynamic.setDefaultLoadingComponent(() => config.router.loading);

// 3. Model
app.model(require('./models/global').default);

// 4. Router
app.router(({ history, app }) => (
  <ConfigProvider locale={en_US}>
    <Router history={history}>{createRoutes(app)}</Router>
  </ConfigProvider>
));

// 5. Start
app.start('#root');

// export global
const glo = {
  app,
  store: app._store,
  dispatch: app._store.dispatch
};
export default glo

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
