import React, { Component } from 'react';
import { connect, router } from 'dva';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Layout, Button, Input, Checkbox, Spin, Form } from 'antd';
import logoImg from 'assets/images/logo.png';
import './index.less';
const { Link } = router;
const { Content } = Layout;
const FormItem = Form.Item;

@connect(({ login, loading }) => ({
    login,
    loading: loading.models.login
}))
class Login extends Component {
    handleSubmit = values => {
        const { dispatch } = this.props;
        dispatch({
            type: 'login/login',
            payload: values
        });
    };

    render() {
        const { loading } = this.props;

        return (
            <Layout className="full-layout login-page">
                <Content>
                    <Spin tip="Logging in..." spinning={!!loading}>
                        <Form onFinish={this.handleSubmit}
                            className="login-form"
                            initialValues={{
                                userName: 'admin',
                                password: 'admin',
                                remember: true
                            }}>
                            <div className="user-img">
                                <img src={logoImg} alt="logo" />
                                <b>PEIHNAT</b>
                            </div>
                            <FormItem name="userName" rules={[{
                                required: true,
                                message: 'Please enter your username'
                            }]}>
                                <Input
                                    size="large"
                                    prefix={<UserOutlined />}
                                    placeholder="Username"
                                />
                            </FormItem>
                            <FormItem name="password" rules={[{
                                required: true,
                                message: 'Please enter your password'
                            }]}>
                                <Input
                                    size="large"
                                    prefix={<LockOutlined />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </FormItem>
                            <FormItem name="remember" valuePropName="checked" noStyle>
                                <Checkbox>Remember me</Checkbox>
                            </FormItem>
                            <Link className="login-form-forgot" to="#">
                                Forgot password
                            </Link>
                            <Button
                                size="large"
                                type="primary"
                                htmlType="submit"
                                className="login-form-button">
                                Login
                            </Button>
                            <div className="new-user">
                                <Link to="/sign/register">Register</Link>
                            </div>
                        </Form>
                    </Spin>
                </Content>
            </Layout>
        );
    }
}
export default Login;