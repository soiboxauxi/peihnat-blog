import { dynamicWrapper, createRoute } from '@/utils/core';

const routesConfig = (app) => ({
    path: '/sign/login',
    title: 'Login',
    component: dynamicWrapper(app, [import('./model')], () => import('./components'))
});

const exp = app => createRoute(app, routesConfig);
export default exp;