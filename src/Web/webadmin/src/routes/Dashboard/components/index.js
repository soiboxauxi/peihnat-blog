import React from 'react';
import { connect } from 'dva';
import BaseComponent from 'components/BaseComponent';
import './index.less';
import { Layout, Col, Row } from 'antd';

const { Content } = Layout;

@connect(({ dashboard }) => ({
    dashboard
}))
class Dashboard extends BaseComponent {
    render() {
        const { dashboard } = this.props;
        return (
            <Layout className="full-layout page dashboard-page">
                <Content>
                    <Row gutter={20}>
                        <Col md={6}>
                            A
                        </Col>
                        <Col md={6}>
                            B
                        </Col>
                        <Col md={6}>
                            C
                        </Col>
                        <Col md={6}>
                            D
                        </Col>
                    </Row>
                </Content>
            </Layout>
        );
    };
}
export default Dashboard;