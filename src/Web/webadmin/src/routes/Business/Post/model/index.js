import modelEnhance from '@/utils/modelEnhance';
import PageHelper from '@/utils/pageHelper';

/**
  * True when the page is loaded for the first time
  * This value can be used to prevent page switching
  * Initialize data multiple times
  */
let LOADED = false;

export default modelEnhance({
  namespace: 'post',

  state: {
    pageData: PageHelper.create(),
    posts: []
  },

  subscriptions: {},

  effects: {},

  reducers: {}
});