import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Layout } from 'antd';
import BaseComponent from 'components/BaseComponent';
import DataTable from 'components/DataTable';
import { ModalForm } from 'components/Modal';
import SearchBar from 'components/SearchBar';
import Toolbar from 'components/Toolbar';
import { connect } from 'dva';
import React from 'react';
import createColumns from './columns';
import '../style/index.less';

const { Content, Header, Footer } = Layout;
const Pagination = DataTable.Pagination;

@connect(({ post, loading }) => ({
    post,
    loading: loading.models.crud
}))
class Post extends BaseComponent {
    state = {
        record: null,
        visible: false,
        rows: []
    };

    render() {
        const { post, loading, dispatch } = this.props;
        const { rows, record, visible } = this.state;
        const {
            pageData,
            posts
        } = post;

        const columns = createColumns(this, posts);

        const searchBarProps = {
            columns,
            //onSearch: values => {
            //    dispatch({
            //        type: 'crud/getPageInfo',
            //        payload: {
            //            pageData: pageData.filter(values).jumpPage(1, 10)
            //        }
            //    });
            //}
        };

        const dataTableProps = {
            columns,
            dataItems: pageData,
        };

        const modalFormProps = {
            loading,
            record,
            visible,
            columns,
            modalOpts: {
                width: 700
            },
            onCancel: () => {
                this.setState({
                    record: null,
                    visible: false
                });
            },
            // New additions and modifications will enter this method,
            // You can use the primary key or whether there is a 
            // record to distinguish the state
            //onSubmit: values => {
            //    dispatch({
            //        type: 'crud/save',
            //        payload: {
            //            values,
            //            success: () => {
            //                this.setState({
            //                    record: null,
            //                    visible: false
            //                });
            //            }
            //        }
            //    });
            //}
        };

        return (
            <Layout className="full-layout crud-page">
                <Header>
                    <Toolbar
                        appendLeft={
                            <Button.Group>
                                <Button
                                    type="primary"
                                    icon={<PlusOutlined />}
                                    onClick={this.onAdd}>
                                    Add
                                </Button>
                                <Button
                                    disabled={!rows.length}
                                    onClick={e => this.onDelete(rows)}
                                    icon={<DeleteOutlined />}>
                                    Delete
                                </Button>
                            </Button.Group>
                        }
                        pullDown={
                            <SearchBar
                                type="grid"
                                {...searchBarProps}
                            />}>
                        <SearchBar
                            group="abc"
                            {...searchBarProps} />
                    </Toolbar>
                </Header>
                <Content>
                    <DataTable {...dataTableProps} />
                </Content>
                <Footer>
                    <Pagination {...dataTableProps} />
                </Footer>
                <ModalForm {...modalFormProps} />
            </Layout>
        );
    };
}

export default Post;