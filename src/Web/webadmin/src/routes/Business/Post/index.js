import { dynamicWrapper, createRoute } from '@/utils/core';

const routesConfig = (app) => ({
    path: '/post',
    title: 'Post',
    component: dynamicWrapper(app,
        [import('./model')],
        () => import('./components')),
    exact: true
});
const exp = (app) => createRoute(app, routesConfig);
export default exp;
