import BasicLayout from '@/layouts/BasicLayout';
import UserLayout from '@/layouts/UserLayout';
import { createRoutes } from '@/utils/core';
import Blank from './Blank';
import Post from './Business/Post';
import Dashboard from './Dashboard';
import Login from './Login';

const routesConfig = app => [
    {
        path: '/sign',
        title: 'Login',
        indexRoute: '/sign/login',
        component: UserLayout,
        childRoutes: [
            Login(app),
            //Register(app),
            //NotFound()
        ]
    },
    {
        path: '/',
        title: 'Dashboard',
        component: BasicLayout,
        indexRoute: '/dashboard',
        childRoutes: [
            Dashboard(app),
            Blank(app),
            Post(app)
        ]
    }
];

const exp = app => createRoutes(app, routesConfig);
export default exp;