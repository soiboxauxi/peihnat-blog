//import { normal } from 'components/Notification';
import PageLoading from 'components/Loading/PageLoading';

// System notification, define what style of notification to use, normal or antdNotice
//const notice = normal;

const config = {
    /**
       * HTML title template
       */
    htmlTitle: 'ADMIN - {title}',

    // Global exception
    exception: {
        global: (err, dispatch) => {
            const errName = err.name;
            console.log(err);
            // RequestError is the interception request exception
            if (errName === 'RequestError') {
                //notice.error(err.message);
                console.error(err);
            } else {
                console.error(err);
            }
        },
    },

    // Asynchronous request configuration
    request: {
        prefix: '/api',

        // Each request header will carry these parameters
        withHeaders: () => ({
            //token: store.getStore("token"),
        }),

        /**
         * Because modelEnhance needs to know the data returned by the server,
         * What is success, what is failure, such as
         * {status: true, data: ...} // represents success
         * {status: false, message: ...} // means failure
         * In practice, the response in the response should be returned by the server
         * Success and failure identification to distinguish
         */
        afterResponse: response => {
            const { status, message } = response;
            if (status) {
                return response;
            } else {
                throw new Error(message);
            }
        },
        errorHandle: err => {
            // Request error global interception
            if (err.name === 'RequestError') {
                //notice.error(err.text || err.message);
            }
        }
    },

    // Route loading effect
    router: {
        loading: <PageLoading loading />
    },

    /**
     * Packing back data when simulating data
     * Because, when the backend returns data, it usually wraps a layer of status information outside
     * If successful:
     * {
     * status: true,
     * data: responseData
     *}
     * Or when an error occurs:
     * {
     * status: false,
     * code: 500,
     * message:'Username or password is wrong'
     *}
     * Here is the configuration of these two functions, in order for us to write a few lines of code less orz...
     */
    mock: {
        toSuccess: response => ({
            status: true,
            data: response
        }),

        toError: message => ({
            status: false,
            message: message
        })
    }
};

export default config;