import { enquireIsMobile } from '@/utils/enquireScreen';
import { Layout } from 'antd';
import cx from 'classnames';
import $$ from 'cmn-utils';
import NavBar from 'components/NavBar';
import { LeftSideBar } from 'components/SideBar';
import TopBar from 'components/TopBar';
import { connect, router } from 'dva';
import pathToRegexp from 'path-to-regexp';
import React from 'react';
import isEqual from 'react-fast-compare';
import { CSSTransition, SwitchTransition } from 'react-transition-group';
import './styles/basic.less';
import TabsLayout from './TabsLayout';

const { Switch } = router;
const { Content, Header } = Layout;
@connect(({ global }) => ({ global }))
class BasicLayout extends React.PureComponent {
    constructor(props) {
        super(props);
        const user = $$.getStore('user', []);
        const theme = $$.getStore('theme', {
            leftSide: 'darkgrey', // left
            navbar: 'light' // top
        });
        if (!theme.layout) {
            theme.layout = [
                'fixedHeader',
                'fixedSidebar',
                'fixedBreadcrumbs'
            ];
        }
        this.state = {
            collapsedLeftSide: false, // Left sidebar switch control
            leftCollapsedWidth: 60, // Left column width
            expandTopBar: false, // Opening and closing of the head multifunctional area
            showSidebarHeader: false, // Left sidebar head switch
            collapsedRightSide: true, // Right sidebar switch
            theme,
            user,
            currentMenu: this.getCurrentMenu(this.props),
            isMobile: false
        };

        props.dispatch({
            type: 'global/getMenu'
        });
    }

    componentDidMount() {
        //this.checkLoginState();

        this.unregisterEnquire = enquireIsMobile(ismobile => {
            const { isMobile, theme } = this.state;
            if (isMobile !== ismobile) {
                // If it is a mobile terminal, the sidebar is not fixed
                if (ismobile && $$.isArray(theme.layout)) {
                    theme.layout = theme.layout.filter(item =>
                        item !== 'fixedSidebar');
                }
                this.setState({
                    isMobile: ismobile
                });
            }
        });
    }

    // Check if a user is logged in
    //checkLoginState() {
    //    const user = $$.getStore('user');
    //    if (!user) {
    //        this.props.dispatch(routerRedux.replace('/sign/login'));
    //    }
    //}

    componentDidUpdate(prevProps, prevState) {
        if (
            !isEqual(this.props.location.pathname, prevProps.location.pathname) ||
            !isEqual(this.props.global.flatMenu, prevProps.global.flatMenu)
        ) {
            this.setState({
                currentMenu: this.getCurrentMenu(this.props) || {}
            });
        }
    }

    componentWillUnmount() {
        // 清理监听
        this.unregisterEnquire();
    }

    getCurrentMenu(props) {
        const {
            location: { pathname },
            global
        } = props || this.props;
        const menu = this.getMeunMatchKeys(global.flatMenu, pathname)[0];
        return menu;
    }

    getMeunMatchKeys = (flatMenu, path) => {
        return flatMenu.filter(item => {
            return pathToRegexp(item.path).test(path);
        });
    };

    /**
     * The top left menu icon shrink control
     */
    onCollapseLeftSide = _ => {
        const collapsedLeftSide =
            this.state.leftCollapsedWidth === 0
                ? true
                : !this.state.collapsedLeftSide;

        this.setState({
            collapsedLeftSide,
            leftCollapsedWidth: 60
        });
    }

    /**
     * Toggle the opening and closing of the head in the left column
     */
    toggleSidebarHeader = _ => {
        this.setState({
            showSidebarHeader: !this.state.showSidebarHeader
        });
    };

    render() {
        const {
            collapsedLeftSide,
            leftCollapsedWidth,
            expandTopBar,
            showSidebarHeader,
            collapsedRightSide,
            theme,
            user,
            currentMenu,
            isMobile
        } = this.state;

        const { routerData, location, global } = this.props;
        const { menu, flatMenu } = global;
        const { childRoutes } = routerData;
        const classnames = cx('basic-layout', 'full-layout', {
            fixed: theme.layout && theme.layout.indexOf('fixedSidebar') !== -1,
            'fixed-header':
                theme.layout && theme.layout.indexOf('fixedHeader') !== -1,
            'fixed-breadcrumbs':
                theme.layout && theme.layout.indexOf('fixedBreadcrumbs') !== -1,
            'hided-breadcrumbs':
                theme.layout && theme.layout.indexOf('hidedBreadcrumbs') !== -1
        });

        return (
            <Layout className={classnames}>
                <Header>
                    <NavBar
                        collapsed={collapsedLeftSide}
                        onCollapseLeftSide={this.onCollapseLeftSide}
                        onExpandTopBar={this.onExpandTopBar}
                        toggleSidebarHeader={this.toggleSidebarHeader}
                        theme={theme.navbar}
                        user={user}
                        isMobile={isMobile} />
                </Header>
                <Layout>
                    <LeftSideBar
                        collapsed={collapsedLeftSide}
                        leftCollapsedWidth={leftCollapsedWidth}
                        showHeader={showSidebarHeader}
                        onCollapse={this.onCollapseLeftSide}
                        onCollapseAll={this.onCollapseLeftSideAll}
                        location={location}
                        theme={theme.leftSide}
                        flatMenu={flatMenu}
                        currentMenu={currentMenu}
                        menu={menu}
                        //menu={[{ path: "1", name: "TEST MENU", hideInMenu: false }]}
                        user={user}
                        isMobile={isMobile} />
                    <Content>
                        {theme.layout.indexOf('tabLayout') >= 0 ? (
                            <TabsLayout
                                childRoutes={childRoutes}
                                location={location} />
                        ) : (
                                <Layout className="full-layout">
                                    <Header>
                                        <TopBar
                                            expand={expandTopBar}
                                            toggleRightSide={this.toggleRightSide}
                                            collapsedRightSide={collapsedRightSide}
                                            onCollapse={this.onCollapseTopBar}
                                            currentMenu={currentMenu}
                                            location={location}
                                            theme={theme}
                                        />
                                    </Header>
                                    <Content style={{ overflow: 'hidden' }}>
                                        <SwitchTransition>
                                            <CSSTransition
                                                key={location.pathname}
                                                classNames="fade"
                                                timeout={500}
                                            >
                                                <Layout className="full-layout">
                                                    <Content className="router-page">
                                                        <Switch location={location}>{childRoutes}</Switch>
                                                    </Content>
                                                </Layout>
                                            </CSSTransition>
                                        </SwitchTransition>
                                    </Content>
                                </Layout>
                            )}
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default BasicLayout