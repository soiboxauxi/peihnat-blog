import $$, { request } from 'cmn-utils';

const REQUEST = '@request';
const REQUEST_SUCCESS = '@request_success';
const REQUEST_ERROR = '@request_error';
/**
 * 如果单纯想改变一个状态可以在页面中用这个action
 * dispatch({
 *   type: 'crud/@change',
 *   payload: {
 *     showModal: true,
 *   },
 *   success: () => {
 *     console.log('state updated!')
 *   }
 * })
 */
const CHANGE_STATE = '@change';
const CHANGE_STATE_SUCCESS = '@change_success';

/**
 * 封装service中的异步方法，如在model中使用
   const url = '/getPageList';
   const pageInfo = yield call(asyncRequest, {...payload, url});
   yield put({
     type: 'getPageListSuccess',
     payload: pageInfo
   });
 * @param {*} payload 
 */
export async function asyncRequest(payload) {
    if (!payload || !payload.url)
        throw new Error('payload require contains url opt');
    /**
     * other中可以配置 method headers data 等参数
     */
    const { url, pageInfo, ...other } = payload;

    // 如果是分页查询 (格式化发送参数)
    //if (pageInfo && pageInfo instanceof PageInfo) {
    //    const { pageNum, pageSize, filters, sorts } = pageInfo;
    //    let data = { pageNum, pageSize, filters, sorts };
    //
    //    if ($$.isFunction(config.pageHelper.requestFormat)) {
    //        data = config.pageHelper.requestFormat(pageInfo);
    //    }
    //    other.data = data;
    //}

    //const _promise = other.method
    //    ? request[other.method.toLowerCase()](url, other.data, other)
    //    : request.send(url, other);

    // 如果是分页查询（格式化反回结果）
    //if (pageInfo && pageInfo instanceof PageInfo) {
    //    return _promise.then(resp => {
    //        if ($$.isFunction(config.pageHelper.responseFormat)) {
    //            const newPageInfo = config.pageHelper.responseFormat(resp);
    //            // 生成新实例，防止新老指向同一个实例问题
    //            return objectAssign(new PageInfo(), pageInfo, newPageInfo);
    //        }
    //    });
    //} else {
    //    return _promise;
    //}
}

export const simpleModel = {
    namespace: $$.randomStr(4),
    enhance: true,
    state: {},
    effects: {},
    reducers: {}
};

const exp = model => {
    const { namespace, state, subscriptions, effects, reducers, enhance } = {
        ...simpleModel,
        ...model
    };

    if (!enhance) {
        return { namespace, state, subscriptions, effects, reducers };
    }
    return {
        namespace,
        state,
        subscriptions,
        effects: {
            // get old effect
            ...effects,
            /**
              * payload If the payload in the form of an array is passed in, the results will be merged and then called once to render
              * success Get a successful callback after the dispatch ends
              * error Get the failed callback after dispatch
              * afterResponse simulates the operation in reduce, which allows us to have the opportunity to process the returned data without side effects
              */
            //*[REQUEST]({ payload, success, error, afterResponse }, { call, put }) {
            //    let _payloads = [];
            //    if ($$.isObject(payload)) {
            //        _payloads.push(payload);
            //    } else if ($$.isArray(payload)) {
            //        _payloads = payload;
            //    }
            //
            //    const resultState = {
            //        success: {},
            //        error: {}
            //    };
            //
            //    for (let i = 0; i < _payloads.length; i++) {
            //        /**
            //          * valueField: The returned result will be received using the value of the valueField field
            //          * notice: pop-up notice
            //          * actionType: If there is an actionType, it means that the reducer has been processed by itself, and the value is actionType + ('_SUCCESS' |'_ERROR')
            //          */
            //        const { valueField, notice, actionType, ...otherPayload } = _payloads[i];
            //
            //        try {
            //            let response = yield call(asyncRequest, otherPayload);
            //
            //            // Process the returned data and simulate the operation in reduce. Don’t write functions with side effects here
            //            if ($$.isFunction(afterResponse)) {
            //                let _r = afterResponse(response);
            //                if (_r) response = _r;
            //            }
            //
            //            // If you need a callback
            //            if (otherPayload.success) {
            //                otherPayload.success(response);
            //            }
            //
            //            // If you need notification function
            //            if (notice) {
            //                //config.notice.success(notice === true ? 'Successful operation' : notice[0]);
            //            }
            //
            //            // If there is an actionType, it means that it has processed the reducer itself
            //            if (actionType) {
            //                yield put({
            //                    type: `${actionType}_SUCCESS`,
            //                    payload: response
            //                });
            //            } else {
            //                // Ready to return value
            //                resultState.success[valueField || '_@fake_'] = response;
            //            }
            //        } catch (e) {
            //            resultState.error['error'] = e;
            //
            //            // If you need internal callback
            //            if ($$.isFunction(otherPayload.error)) {
            //                otherPayload.error(e);
            //            } else if ($$.isFunction(error)) {
            //                error(e);
            //            }
            //
            //            // Notify the reducer if there is an actionType, it means that the reducer has been processed by itself
            //            yield put({
            //                type: actionType ? `${actionType}_ERROR` : REQUEST_ERROR,
            //                payload: resultState.error
            //            });
            //            // If an error occurs early termination
            //            break;
            //        }
            //    }
            //
            //    // Notify reducer
            //    if (Object.keys(resultState.success).length) {
            //        // If you need a callback
            //        if ($$.isFunction(success)) {
            //            success(resultState.success);
            //        }
            //
            //        yield put({
            //            type: REQUEST_SUCCESS,
            //            payload: resultState.success
            //        });
            //    }
            //},

            //*[CHANGE_STATE]({ payload, success }, { put }) {
            //    yield put({
            //        type: CHANGE_STATE_SUCCESS,
            //        payload
            //    });
            //
            //    if ($$.isFunction(success)) {
            //        success();
            //    }
            //}

        },
        reducers: {
            // get old reducers
            ...reducers,
            // append new request reducers
            [REQUEST_SUCCESS]: _changeState,
            [REQUEST_ERROR]: _changeState,
            [CHANGE_STATE_SUCCESS]: _changeState
        }
    }
};

const _changeState = (state, { payload }) => ({
    ...state,
    ...payload
});

export default exp;