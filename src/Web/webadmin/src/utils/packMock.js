// http://www.wheresrhys.co.uk/fetch-mock/api
// http://mockjs.com/
import fetchMock from 'fetch-mock';
import $$ from 'cmn-utils';
import Mock from 'mockjs';
import config from '@/config';

const mock = Mock.mock;

/**
  * Simulate delay request
  * @param {any} response simulated response data
  * @param {number} time How many milliseconds is the delay time, omitting this number will generate a delay within 100ms
  */
const delay = (response, time) => {
    return () => $$.delay(time || Math.random() * 100).then(() => response);
};

// Packing back data when simulating data
const toSuccess = (response, time) => {
    if (time) {
        return delay(config.mock.toSuccess(response), time);
    } else {
        return config.mock.toSuccess(response);
    }
};
const toError = (message, time) => {
    if (time) {
        return delay(config.mock.toError(message), time);
    } else {
        return config.mock.toError(message);
    }
};

const exp = (...mocks) => {

    /**
     * If the configuration is not intercepted, go directly to the native fetch method
     */

    fetchMock.config = {
        ...fetchMock.config,
        fallbackToNetwork: true,
        warnOnFallback: false
    };

    mocks.forEach(mockFile => {
        let mockAPIs = {};
        if ($$.isFunction(mockFile)) {
            mockAPIs = mockFile({ fetchMock, delay, mock, toSuccess, toError });
        } else if ($$.isObject(mockFile)) {
            mockAPIs = mockFile;
        } else {
            throw new Error('mock file require both Function or Object');
        }


        for (const key in mockAPIs) {
            const method_url = key.split(' ');

            // 'GET /api/getUserInfo'
            let method = 'mock';
            let url = null;
            if (method_url.length === 2) {
                method = method_url[0].toLowerCase();
                url = method_url[1];
            } else {
                url = method_url[0];
            }

            // Deal with the regular situation, that is, with regexp: at the beginning of the URL
            if (url.indexOf('regexp:') === 0) {
                url = new RegExp(url.substring(7));
            }

            /**
              * If you want to return different data for the parameters in the request, such as page turning
              * When analyzing the number of pages in the body, or query conditions, return the corresponding data,
              * At this time, the mock can be written as a function, and the fetch will be received at this time
              * options as parameters fetch(url, options)
              */
            if ($$.isFunction(mockAPIs[key])) {
                fetchMock[method](url, (url, options) =>
                    mockAPIs[key]({ url, ...options })
                );
            } else {
                fetchMock[method](url, mockAPIs[key]);
            }
        }
    });
};
export default exp;

export { mock };