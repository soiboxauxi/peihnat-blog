import $$ from 'cmn-utils';
import PageHelper from './index';
/**
 * Paging object
 */
export default class PageInfo {
    // page number, starting from 1
    pageNum = 1;

    // number per page
    pageSize = 10;

    // Number of current page
    size = 0;

    // total
    total = 0;

    // total pages
    totalPages = 0;

    // result set
    list = [];

    // Filter condition {name:'jonn'}
    filters = {};

    // Sort criteria {name:'asc', age:'desc'}
    sorts = {};

    /**
     * I hope that the number of pages entered by the user is not in the legal range (outside the first page to the last page)
     * When you can correctly respond to the correct result page, then you can configure reasonable to true,
     * At this time, if pageNum<1, the first page will be queried, if pageNum> the total number of pages, the last page will be queried
     */
    reasonable = false;

    /**
     * Assembly page information
     * @param {number} pageNum page number, default 1
     * @param {number} pageSize page size, default 10
     */
    startPage(pageNum = 1, pageSize = 10) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.size = 0;
        this.total = 0;
        this.totalPages = 0;
        this.list = [];
        this.filters = {};
        this.sorts = {};
        return this;
    }

    /**
     * Assembly page information
     * @param {number} pageNum page number
     * @param {number} pageSize page size
     */
    jumpPage(pageNum, pageSize) {
        if ((pageNum && pageNum <= Math.ceil(this.totalPages)) || pageNum === 1) {
            this.pageNum = pageNum;
            if (pageSize) this.pageSize = pageSize;
        }
        return this;
    }

    /**
     * Splicing filter conditions
     * @param {object} q filter criteria {name:'jonn', sex: 1}
     * @param {boolean} merge whether to merge the new condition with the existing condition
     */
    filter(q, merge) {
        if ($$.isObject(q)) {
            if (merge) {
                this.filters = { ...this.filters, ...q };
            } else {
                this.filters = q;
            }
        }
        return this;
    }

    /**
     * Concatenation sort condition
     * @param {object} q Sort field {name:'asc', age:'desc'}
     */
    sortBy(q) {
        if ($$.isObject(q)) {
            this.sorts = q;
        }
        return this;
    }

    /**
     * Next page or specified number of pages
     * @param {number} pageNum
     */
    nextPage(pageNum) {
        if (this.totalPages !== -1) {
            if (pageNum && pageNum <= Math.ceil(this.totalPages)) {
                this.pageNum = pageNum;
            } else if (this.pageNum + 1 <= Math.ceil(this.totalPages)) {
                this.pageNum++;
            }
        } else {
            this.pageNum = this.totalPages;
        }
        return this;
    }

    /**
     * Previous
     */
    prevPage() {
        if (this.totalPages !== -1) {
            if (this.pageNum - 1 > 0) {
                this.pageNum--;
            }
        } else {
            this.pageNum = 1;
        }
        return this;
    }

    // deprecate
    send(url, options) {
        const self = this;
        const { pageNum, pageSize, filters, sorts } = this;
        let data = { pageNum, pageSize, filters, sorts };

        if ($$.isFunction(PageHelper.requestFormat)) {
            data = PageHelper.requestFormat(this);
        }
        return $$.send(url, { data, ...options }).then(resp => {
            if ($$.isFunction(PageHelper.responseFormat)) {
                const newPageInfo = PageHelper.responseFormat(resp);
                return Object.assign(self, newPageInfo);
            }
        })
    }
}