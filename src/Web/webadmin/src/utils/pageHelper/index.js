import PageInfo from './PageInfo';
import config from '@/config';

/**
 * Universal paging assistant
 */
export default class PageHelper {
    static create = () => {
        const pageInfo = new PageInfo();
        return pageInfo;
    }

    /**
     * You can format the parameters sent to the backend by setting this function
     *
     * For example, the parameters required by the back-end paging interface are
     * {
     * currentPage: 1,
     * showCount: 10,
     * paramMap: {name:'jonn'}
     *}
     * The paging information can be formatted by setting this parameter
     * E.g:
     * pageHelper.requestFormat(({pageNum, pageSize}) => ({
     * currentPage: pageNum,
     * showCount: pageSize
     * }))
    */
    static requestFormat(pageInfo) {
        return config.pageHelper.requestFormat(pageInfo);
    };


    /**
     * Format the data returned from the server and put it into the PageInfo object,
     * Prepare for the next page break
     * Page number pageNum;
       Number per page pageSize;
       The number of the current page size;
       Total number of records total;
       Total pages totalPages;
       Result set list;
     * @param {object} resp
    */
    static responseFormat(resp) {
        return config.pageHelper.responseFormat(resp);
    }
}