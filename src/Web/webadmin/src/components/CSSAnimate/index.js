import React, { PureComponent } from 'react';
import cx from 'classnames';
import omit from 'object.omit';

class CSSAnimate extends PureComponent {
    static defaultProps = {
        component: 'div'
    };

    render() {
        const {
            className,
            children,
            delay,
            duration,
            style,
            component,
            ...otherProps
        } = this.props;
        const Component = component;
        const classnames = cx('animated', className);
        const _style = { ...style };

        if (duration) {
            _style.animationDuration = duration + 'ms';
            _style.WebkitAnimationDuration = duration + 'ms';
        }

        if (delay) {
            _style.animationDelay = delay + 'ms';
            _style.WebkitAnimationDelay = delay + 'ms';
        }

        const divProps = omit(otherProps, [
            'type',
            'callback',
            'delay',
            'duration'
        ]);

        return (
            <Component
                className={classnames}
                {...divProps}
                style={_style}>
                {children}
            </Component>
        );
    };
}

export default CSSAnimate;