import React from 'react';
import './PageLoading.less'

/**
 * Loading effect
 */
const exp = ({ loading, style = 'style1' }) =>
    loading ?
        <div className={`loading-spinner loading-spinner-${style}`}></div>
        : null;
export default exp;