import { Avatar, Badge, Popover } from 'antd';
import avatarImg from 'assets/images/avatar.jpg';
import logoImg from 'assets/images/logo.png';
import cx from 'classnames';
import { router } from 'dva';
import React, { PureComponent } from 'react';
import Icon from '../Icon';
import './style/index.less';

const { Link } = router;
/**
  * The head area of this bureau
  */
class NavBar extends PureComponent {
    state = {
        openSearchBox: false
    };

    static defaultProps = {
        fixed: true,
        theme: '' //'bg-dark',
    };

    toggleFullScreen() {
        if (
            !document.fullscreenElement &&
            !document.mozFullScreenElement &&
            !document.webkitFullscreenElement &&
            !document.msFullscreenElement
        ) {
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(
                    Element.ALLOW_KEYBOARD_INPUT
                );
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    }

    render() {
        const {
            fixed,
            theme,
            user,
            onCollapseLeftSide,
            collapsed,
            onExpandTopBar,
            toggleSidebarHeader,
            isMobile
        } = this.props;

        const classnames = cx('navbar', {
            'navbar-fixed-top': !!fixed,
            'navbar-sm': isMobile ? true : collapsed,
            ['bg-' + theme]: !!theme
        });

        return (
            <header className={classnames}>
                <div className="navbar-branding">
                    <Link className="navbar-brand" to="/">
                        <img src={logoImg} alt="logo" />
                        <b>PEIHNAT</b>
                    </Link>
                    <span className="toggle_sidemenu_l"
                        onClick={onCollapseLeftSide}>
                        <Icon type="lines" />
                    </span>
                </div>
                <ul className="nav navbar-nav navbar-left clearfix">
                    {
                        collapsed || isMobile ? null : (
                            <li>
                                <a className="sidebar-menu-toggle"
                                    href="#/"
                                    onClick={toggleSidebarHeader}>
                                    <Icon type="ruby" />
                                </a>
                            </li>
                        )
                    }
                    <li>
                        <a href="onExpandTopBar"
                            onClick={onExpandTopBar}>
                            <Icon type="wand" />
                        </a>
                    </li>
                    {isMobile ? (
                        <li className="mini-search"
                            onClick={this.onOpenSearchBox}>
                            <a href="search">
                                <Icon type="search" antd />
                            </a>
                        </li>
                    ) : (
                            <li onClick={this.toggleFullScreen}>
                                <a className="request-fullscreen"
                                    href="#/">
                                    <Icon type="screen-full" />
                                </a>
                            </li>
                        )}
                </ul>
                {isMobile ? null : (
                    <form className="navbar-form navbar-search clearfix">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Search"
                                onClick={this.onOpenSearchBox}
                            />
                        </div>
                    </form>
                )}
                <ul className="nav navbar-nav navbar-right clearfix">
                    <li>
                        <a href="https://github.com/LANIF-UI/dva-boot-admin">
                            <Icon type="GithubOutlined" antd />
                        </a>
                    </li>
                    <li className="dropdown">
                        <Popover
                            placement="bottomRight"
                            title={'Notice'}
                            overlayClassName={cx('navbar-popup',
                                { [theme]: !!theme })}
                            content={''}
                            trigger="click"
                        >
                            <a className="dropdown-toggle"
                                href="dropdown-toggle">
                                <Icon type="radio-tower" />
                            </a>
                        </Popover>
                    </li>
                    <li className="dropdown">
                        <Popover
                            placement="bottomRight"
                            title={`WELCOME ${user.userName}`}
                            overlayClassName={cx('navbar-popup',
                                { [theme]: !!theme })}
                            content={<UserDropDown />}
                            trigger="click">
                            <a className="dropdown-toggle"
                                href="#/">
                                <Badge dot>
                                    <Avatar src={avatarImg}>
                                        {user.userName}
                                    </Avatar>
                                </Badge>
                            </a>
                        </Popover>
                    </li>
                </ul>
            </header>
        )
    }
}

const UserDropDown = props => (
    <ul className="dropdown-menu list-group dropdown-persist">
        <li className="list-group-item">
            <a className="animated animated-short fadeInUp"
                href="#/">
                <Icon type="mail" /> Mail
    <Badge count={5} className="label" />
            </a>
        </li>
        <li className="list-group-item">
            <a className="animated animated-short fadeInUp"
                href="#/">
                <Icon type="users" /> User
    <Badge count={6} className="label" />
            </a>
        </li>
        <li className="list-group-item">
            <a className="animated animated-short fadeInUp"
                href="#/">
                <Icon type="gear" /> Account setting
  </a>
        </li>
        <li className="list-group-item">
            <a className="animated animated-short fadeInUp"
                href="#/">
                <Icon type="ring" /> Notice
  </a>
        </li>
        <li className="list-group-item dropdown-footer">
            <Link to="/sign/login">
                <Icon type="poweroff" /> Logout
  </Link>
        </li>
    </ul>
);

export default NavBar;