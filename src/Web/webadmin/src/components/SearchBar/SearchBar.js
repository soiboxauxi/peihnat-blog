import { Form } from '@ant-design/compatible';
import cx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { Row, Col, Button, message } from 'antd';
import { ReloadOutlined, SearchOutlined } from '@ant-design/icons';
import './style/index.less';

const createForm = Form.create;
const PlainComp = ({ className, children }) => (
    <div className={className}>{children}</div>
);
PlainComp.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

/**
 * Search bar
 */
class SearchBar extends React.Component {
    static propTypes = {
        prefixCls: PropTypes.string,
        className: PropTypes.string,
        /**
         * See the help document column.js usage for details
         */
        columns: PropTypes.array.isRequired,
        /**
         * Use record data to assign values ​​to the form {key:value, key1: value1}, the initial value of the time type needs to be converted to the moment type
         */
        record: PropTypes.object,
        /**
         * Search bar type inline (inline), grid (grid)
         */
        type: PropTypes.string,
        /**
         * Search criteria grouping. After setting this attribute, search items with the same group value will be filtered in column.js
         */
        group: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        /**
         * Same as Row configuration in Grid component in antd
         */
        rows: PropTypes.object,
        /**
         * Same as Col configuration in Grid component in antd
         */
        cols: PropTypes.object,
        /**
         * Additional search terms
         */
        children: PropTypes.node,
        /**
         * How to deal with items with pop-up boxes in the form, such as drop-down boxes, drop-down trees, date selection, etc.
         * If set to true, it will be automatically attached to the form, or specified by function
         * For usage, see the getPopupContainer property of antd with pop-up window component http://ant-design.gitee.io/components/select-cn/
         * appendTo={triggerNode => triggerNode.parentNode}
         */
        appendTo: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),

        form: PropTypes.object,

        /**
         * Click the query button onSearch(values, isReset) values ​​to query whether the data isReset is reset
         */
        onSearch: PropTypes.func
    };

    static defaultProps = {
        prefixCls: 'antui-searchbar',
        type: 'inline'
    };

    // When type is grid, specify the number of elements in each row
    cols = {
        xs: 8,
        md: 6,
        xl: 4
    };

    // Default width of inline elements
    width = {
        date: 100,
        month: 100,
        'date~': 280,
        datetime: 140,
        select: 100,
        default: 110,
        treeSelect: 110,
        cascade: 110,
        cascader: 110
    };

    // When type is grid, specify the interval between every two elements
    rows = {
        gutter: 8
    };

    render() {
        const {
            className,
            prefixCls,
            type,
            rows,
            cols,
            columns,
            group,
            children,
            form,
            appendTo,
            record,
            ...otherProps
        } = this.props;

        const colopts = type === 'grid' ? cols || this.cols : {};
        const rowopts = type === 'grid' ? rows || this.rows : {};

        let ComponentRow = type === 'inline' ? PlainComp : Row;
        let ComponentCol = type === 'inline' ? PlainComp : Col;
        let ComponentItem = type === 'inline' ? PlainComp : Form.Item;

        let ComponentBtnGroup = type === 'inline' ? Button.Group : PlainComp;

        let searchFields = columns.filter(col => col.searchItem);

        return (
            <div className={cx(prefixCls, className)} {...otherProps}>
                <Form
                    className={cx({
                        'form-inline': type === 'inline',
                        'form-grid': type === 'grid'
                    })}
                >
                    <ComponentRow className="row-item" {...rowopts}>
                        {
                            searchFields.map((field, i) => {

                            })
                        }
                    </ComponentRow>
                    <ComponentBtnGroup className="search-btns">
                        <Button
                            title="Search"
                            type={type === 'grid' ? 'primary' : 'default'}
                            onClick={e => this.searchForm()}
                            htmlType="submit"
                            icon={<SearchOutlined />}>
                            Search
                        </Button>
                        <Button
                            title="Default"
                            onClick={e => this.resetForm()}
                            icon={<ReloadOutlined />}>
                            Default
                        </Button>
                    </ComponentBtnGroup>
                </Form>
            </div>
        );
    };
};

export default createForm()(SearchBar);