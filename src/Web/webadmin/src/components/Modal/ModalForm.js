import React, { Component } from 'react';
import './style/index.less';
import PropTypes from 'prop-types';
import { Modal, Button } from 'antd';
import Form from '../Form';
import cx from 'classnames';

class ModalForm extends Component {
    static propTypes = {
        title: PropTypes.string,
        record: PropTypes.object,
        visible: PropTypes.bool,
        columns: PropTypes.array,
        onCancel: PropTypes.func,
        onSubmit: PropTypes.func,
        modalOpts: PropTypes.object,
        formOpts: PropTypes.object,
        className: PropTypes.string
    };

    state = {
        visible: false
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.visible !== prevState.visible) {
            return {
                visible: nextProps.visible
            };
        }
        return null;
    }

    closeModal = () => {
        if (this.props.onCancel) {
            this.props.onCancel();
            return;
        }
        this.setState({
            visible: false
        });
    };

    render() {
        const {
            title,
            record,
            className,
            columns,
            onCancel,
            onSubmit,
            modalOpts,
            formOpts,
            loading,
            full,
            preview
        } = this.props;

        const classname = cx(className,
            'antui-modalform',
            {
                'full-modal': full
            });

        const modalProps = {
            className: classname,
            visible: this.state.visible,
            title: title || (record ? 'Edit' : 'Add'),
            onCancel: this.closeModal,
        };

        const formProps = {
            ref: 'form',
            columns,
        };
        return (
            <Modal {...modalProps}>
                <Form {...formProps} />
            </Modal>
        );
    };
}

export default ModalForm;