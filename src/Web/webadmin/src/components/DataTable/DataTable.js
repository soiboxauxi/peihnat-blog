import React, { Component } from 'react';
import { Table, Pagination, Tooltip } from 'antd';
import PropTypes from 'prop-types';
import cx from 'classnames';
import objectAssign from 'object-assign';

/**
 * Data table
 */
class DataTable extends Component {
    static propTypes = {
        prefixCls: PropTypes.string,
        className: PropTypes.string,
        rowKey: PropTypes.string,
        /**
     * See the help document column.js usage for details
     */
        columns: PropTypes.array.isRequired,
        /**
         * The data object list is required. If the table comes with paging, you need to provide paging information here {pageNum:1, list:[], filters:{}, pageSize:10, total:12}
         */
        dataItems: PropTypes.object.isRequired,
        /**
         * Whether to display the row number
         */
        showNum: PropTypes.bool,
        /**
         * Whether the odd and even rows have different colors
         */
        alternateColor: PropTypes.bool,
        /**
         * Multiple choice/single choice, checkbox or radio
         */
        selectType: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
        /**
         * The configuration of the selection function refers to the rowSelection configuration item of antd
         */
        rowSelection: PropTypes.object,
        /**
         * Specify the key array of the selected item
         */
        selectedRowKeys: PropTypes.array,
        /**
         * Whether with a scroll bar, or as a scroll parameter
         */
        isScroll: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
        /**
         * Whether to increase the paging in the form
         */
        pagination: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
        /**
         * Selected table row callback function(selectedRowKeys, selectedRows)
         */
        onSelect: PropTypes.func,
        /**
         * External data acquisition interface {pageNum:1, filters:{}, pageSize:10}
         */
        onChange: PropTypes.func
    };

    static defaultProps = {
        prefixCls: 'antui-datatable',
        alternateColor: true
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedRowKeys: props.selectedRowKeys,
            selectedRows: this.getSelectedRows(props.selectedRowKeys),
            tableHeight: null
        };
    }

    // Convert value to object array
    getSelectedRows(value, oldValue = [], rowKey) {

    }

    render() {
        const {
            prefixCls,
            className,
            columns,
            dataItems,
            showNum,
            alternateColor,
            onChange,
            selectType,
            rowSelection,
            isScroll,
            pagination,
            rowKey,
            ...otherProps
        } = this.props;

        let classname = cx(prefixCls, className, {
            'table-row-alternate-color': alternateColor
        });

        let colRowKey = '';
        // Default width
        let cols = columns;

        const _rowSelection = {
            type: selectType === 'radio' ? 'radio' : 'checkbox',
            selectedRowKeys: this.state.selectedRowKeys,
            onChange: this.onSelectChange,
            ...rowSelection
        };

        // Pagination
        const paging = objectAssign({});

        this._rowKey = rowKey || colRowKey;

        return (
            <div className={classname}>
                <Table
                    //size="small"
                    //rowSelection={selectType ? _rowSelection : null}
                    //onRow={
                    //    selectType
                    //        ? (record, index) => ({
                    //            onClick: _ => this.tableOnRow(record, index)
                    //        })
                    //        : () => { }
                    //}
                    //scroll={
                    //    isScroll ?
                    //        objectAssign({ x: '100%' }, isScroll) : {}}
                    //bodyStyle={{ overflowX: 'auto' }}
                    columns={cols}
                    //pagination={pagination ? paging : false}
                    dataSource={dataItems.list}
                //onChange={this.handleTableChange}
                //rowKey={this._rowKey}
                //{...otherProps}
                />
            </div>
        );
    };
}

export const Paging = () => {
    const paging = {};
    return <Pagination defaultCurrent={6} total={500} />;
};

DataTable.Pagination = Paging;

export default DataTable;