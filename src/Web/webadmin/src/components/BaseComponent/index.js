import React from 'react';

class BaseComponent extends React.Component {
    /**
     * Add
     */
    onAdd = () => {
        this.setState({
            record: null,
            visible: true
        });
    };
}

export default BaseComponent;