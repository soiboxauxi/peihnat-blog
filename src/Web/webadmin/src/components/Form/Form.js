import { Form } from '@ant-design/compatible';
import '@ant-design/compatible/assets/index.css';
import cx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import './style/index.less';
import objectAssign from 'object-assign';
import { Row, Col, Button, Divider } from 'antd';

const createForm = Form.create;

const PlainComp = ({ className, children }) => (
    <div className={className}>{children}</div>
);
PlainComp.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

/**
 * Form component
 */
class FormComp extends React.Component {
    static propTypes = {
        prefixCls: PropTypes.string,
        className: PropTypes.string,
        /**
         * See the help document column.js usage for details
         */
        columns: PropTypes.array.isRequired,
        /**
         * Use record data to assign values to the form 
         * {key:value, key1: value1}, the initial value 
         * of the time type needs to be converted to the 
         * moment type
         */
        record: PropTypes.object,
        /**
         * Form type inline (inline), grid (grid)
         */
        type: PropTypes.string,
        /**
         * Search criteria grouping. After setting this attribute, search 
         * items with the same group value will be filtered in column.js
         */
        group: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        /**
         * How to deal with items with pop-up boxes in the form, such as 
         * drop-down boxes, drop-down trees, date selection, etc.
         * If set to true, it will be automatically attached to the form, 
         * or specified by function
         * For usage, see the getPopupContainer property of antd with 
         * pop-up window component 
         * http://ant-design.gitee.io/components/select-cn/
         * appendTo={triggerNode => triggerNode.parentNode}
         */
        appendTo: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
        /**
         * Same as Row configuration in Grid component in antd
         */
        rows: PropTypes.object,
        /**
         * Same as Col configuration in Grid component in antd
         */
        cols: PropTypes.object,
        /**
         * Additional form items
         */
        children: PropTypes.node,
        /**
         * antd's form object
         */
        form: PropTypes.object,
        /**
         * Click the query button onSubmit(values) values ​​to submit data
         */
        onSubmit: PropTypes.func,

        /**
         * Whether it is preview view, all form items will be displayed 
         * in text mode
         */
        preview: PropTypes.bool,

        /** antd formItemLayout */
        formItemLayout: PropTypes.object,
        layout: PropTypes.object, // same as formItemLayout

        /**
         * Is it submitted?
         */
        loading: PropTypes.bool,

        /**
         * Whether to display the bottom button or pass in a custom 
         * bottom button
         */
        footer: PropTypes.oneOfType([PropTypes.bool, PropTypes.node])
    };

    static defaultProps = {
        prefixCls: 'antui-form',
        type: 'grid',
        loading: false,
        formItemLayout: {
            labelCol: { span: 6 },
            wrapperCol: { span: 17 }
        }
    };

    render() {
        const {
            className,
            prefixCls,
            type,
            rows,
            cols,
            formItemLayout: _formItemLayout,
            layout,
            appendTo,
            columns,
            record,
            group,
            children,
            form,
            preview,
            loading,
            footer,
            ...otherProps
        } = this.props;

        let classname = cx(prefixCls, className, {
            'form-inline': type === 'inline',
            'form-grid': type === 'grid',
            preview: preview
        });

        const colopts = type === 'grid' ? cols || this.cols : {};
        const rowopts = type === 'grid' ? rows || this.rows : {};

        let ComponentRow = type === 'inline' ? PlainComp : Row;
        let ComponentCol = type === 'inline' ? PlainComp : Col;
        let ComponentItem = Form.Item;

        let formFields = columns.filter(col => col.formItem);
        formFields = group
            ? formFields.filter(col => col.formItem && col.formItem.group === group)
            : formFields;

        return (
            <Form
                className={classname}
                onSubmit={this.onSubmit}
                {...objectAssign(
                    otherProps,
                    type === 'inline' && { layout: 'inline' })}>
                <ComponentRow className="row-item" {...rowopts}>
                    {formFields.map((field, i) => {
                        // Pass in a personalized column size, 
                        // change this value to change the number 
                        // of elements in each row
                        let col = { ...colopts };
                        if (type === 'grid' && field.formItem.col) {
                            col = field.formItem.col;
                        } else if (type !== 'grid') {
                            col = {};
                        }

                        let formItemLayout = { ..._formItemLayout, ...layout };
                        if (
                            type === 'grid' &&
                            (field.formItem.formItemLayout || field.formItem.layout)
                        ) {
                            formItemLayout = {
                                ...formItemLayout,
                                ...field.formItem.formItemLayout,
                                ...field.formItem.layout
                            };
                        } else if (type !== 'grid') {
                            formItemLayout = {};
                        }

                        const fieldType = field.formItem.type || 'input';

                        let formProps = {
                            form,
                            name: field.name,
                            title: field.title,
                            record,
                            preview,
                            ...field.formItem
                        };

                        let FieldComp;
                        switch (fieldType) {
                            case 'input': // Input box
                            case 'textarea': // Multiline text
                                FieldComp = require(`./model/input`).default(formProps);
                                break;
                            default:
                                // Universal
                                FieldComp =
                                    require(`./model/${fieldType.toLowerCase()}`)
                                        .default(
                                            formProps
                                        );
                        }
                        
                        return (
                            <ComponentCol
                                key={`col-${i}`}
                                className="col-item"
                                {...col}>
                                <ComponentItem
                                    {...formItemLayout}
                                    label={field.title}
                                    className="col-item-content">
                                    {FieldComp}
                                </ComponentItem>
                            </ComponentCol>
                        );
                    })}
                    {children}
                    {footer === undefined ?
                        (
                            <div></div>
                        ) : (
                            footer
                        )}
                </ComponentRow>
            </Form>
        );
    };
}

export const Item = Form.Item;

export default createForm()(FormComp);