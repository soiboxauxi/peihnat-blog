import React from 'react';
import { Input } from 'antd';
import $$ from 'cmn-utils';
const { TextArea } = Input;

/**
  * Text box element
  */
const exp = ({
    form,
    name,
    formFieldOptions = {},
    record,
    initialValue,
    normalize,
    rules,
    onChange,
    type,
    preview,
    placeholder,
    getPopupContainer,
    ...otherProps }) => {
    const { getFieldDecorator } = form;

    const Comp = type === 'textarea' ? TextArea : Input;

    delete otherProps.render;

    const props = {
        autoComplete: 'off',
        type,
        placeholder: placeholder || `Input ${otherProps.title}`,
        ...otherProps
    };

    return getFieldDecorator(name, formFieldOptions)(<Comp {...props} />);
};
export default exp;
