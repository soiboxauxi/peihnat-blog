import packMock from '@/utils/packMock';
import user from './user';

/**
 * Load mock file
 * packMock(mock1[,mock2])
 */
packMock(
    user,
);