/**
  * Simulate request data
  * @param {FetchMock} fetchMock When the existing conditions are not met, you can use fetchMock to expand
  * @param {function} delay increase the delay time ms Example: delay(mockData) or delay(mockData, 200)
  * @param {function} mock uses mock to generate data, for example:

    mock({
      'string|1-10':'★' // Generate at least 1 and at most 10 star characters
    })

    // {'string':'★★★★★★'}

   For more usage refer to http://mockjs.com/examples.html
  */

const exp = ({ fetchMock, delay, mock, toSuccess, toError }) => {
  // If the existing extension does not meet the needs, you can directly use the fetchMock method
  // fetchMock.mock(/httpbin.org\/post/, {/* response */}, {/* options */});

  return {
    '/api/user/menu': options => toSuccess([
      {
        name: 'Dashboard',
        icon: 'DashboardOutlined',
        path: '/dashboard',
      },
      {
        name: 'Post',
        icon: 'FolderOutlined',
        path: '/post',
      },
      {
        name: 'Component',
        icon: 'DesktopOutlined',
        path: '/component',
        children: [
          {
            name: 'Toolbar',
            path: '/toolbar',
          },
          {
            name: 'BaseComponent',
            path: '/baseComponent',
          },
        ],
      },
    ], 400)
  };
}

export default exp;