/**
  * mock-server uses express to generate real mock data
  * In order to solve the problem that the previous mock data is only valid for fetch, and the request cannot be seen in the network panel of the developer tool
  */

exports.addMockServer = () => config => {
    config.before = app => {
        app.get('/test/get', (req, res) => {
            res.json({ get: 'response get' });
        });
    };
    return config;
};

