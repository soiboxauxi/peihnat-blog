import React, { Suspense } from 'react';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.css';
import PrivateRoute from "./components/PrivateRoute/index";
import Dashboard from './features/Dashboard/index';
import Home from './features/Home/index';
import Login from './features/Login/index';
import Register from './features/Register/index';

function App() {
  return (
    <div className="app">
      <Suspense fallback={<div>Loading...</div>}>
        <BrowserRouter>
          <Switch>
            <Route exact component={Home} path={'/'} />
            <Route component={Login} path={'/login'} />
            <Route component={Register} path={'/register'} />
            {/* <PrivateRoute component={Dashboard} path={'/dashboard'} loginPath={'/login'} exact /> */}
            <Route component={Dashboard} path={'/dashboard'} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}

export default hot(App);
