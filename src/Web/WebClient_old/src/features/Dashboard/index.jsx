import {
  faBars,
  faSearch,
  faUser,
  faTachometerAlt,
  faListAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Suspense, useEffect, useState } from "react";
import { authService } from "../../services/auth/index";
import "./style.scss";
import Home from "./components/Home/index";
import ListPost from "./components/ListPost/index";
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";

Dashboard.propTypes = {};

function Dashboard(props) {
  const [componetCurrent, setComponetCurrent] = useState();
  const [isLoading, setLoading] = useState(false);

  const handleButtonClick = () => {
    authService.logout();
  };

  const handleNavbarClick = (componet) => {
    console.log(componet);
    //  setComponetCurrent(componet);
  };

  const path = window.location.href;
  console.log(path);

  //useEffect((componetCurrent) => {
  //  console.log(componetCurrent);
  //}, componetCurrent);

  return (
    <div className="sb-nav-fixed">
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand" href="index.html">
          Start Bootstrap
        </a>
        <button
          className="btn btn-link btn-sm order-1 order-lg-0"
          id="sidebarToggle"
          href="#"
        >
          <FontAwesomeIcon icon={faBars} />
        </button>

        {/* Navbar Search */}
        <form className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
          <div className="input-group">
            <input
              className="form-control"
              type="text"
              placeholder="Search for..."
              aria-label="Search"
              aria-describedby="basic-addon2"
            />
            <div className="input-group-append">
              <button className="btn btn-primary" type="button">
                <FontAwesomeIcon icon={faSearch} />
              </button>
            </div>
          </div>
        </form>
        {/* Navbar */}
        <ul className="navbar-nav ml-auto ml-md-0">
          <li className="nav-item dropdown">
            <a
              className="nav-link dropdown-toggle"
              id="userDropdown"
              href="https://www.google.com/"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <FontAwesomeIcon icon={faUser} />
            </a>
            <div
              className="dropdown-menu dropdown-menu-right"
              aria-labelledby="userDropdown"
            >
              <a className="dropdown-item" href="https://www.google.com/">
                Settings
              </a>
              <a className="dropdown-item" href="https://www.google.com/">
                Activity Log
              </a>
              <div className="dropdown-divider"></div>
              <a className="dropdown-item" href="login.html">
                Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
          <nav
            className="sb-sidenav accordion sb-sidenav-dark"
            id="sidenavAccordion"
          >
            <div className="sb-sidenav-menu">
              <div className="nav">
                <div className="sb-sidenav-menu-heading">Core</div>
                <div className="nav-link">
                  <div className="sb-nav-link-icon">
                    <FontAwesomeIcon icon={faTachometerAlt} />
                  </div>
                  <div onClick={handleNavbarClick("a")}>Dashboard</div>
                </div>
                <div className="sb-sidenav-menu-heading">Bài viết</div>
                <Link className="nav-link collapsed" to="/dashboard/listpost">
                  <div className="sb-nav-link-icon">
                    <i className="fas fa-columns"></i>
                    <FontAwesomeIcon icon={faListAlt} />
                  </div>
                  Danh sách bài viết
                  <div className="sb-sidenav-collapse-arrow">
                    <i className="fas fa-angle-down"></i>
                  </div>
                </Link>
                <a
                  className="nav-link collapsed"
                  href="#"
                  data-toggle="collapse"
                  data-target="#collapsePostLayouts"
                  aria-expanded="false"
                  aria-controls="collapsePostLayouts"
                >
                  <div className="sb-nav-link-icon">
                    <i className="fas fa-columns"></i>
                    <FontAwesomeIcon icon={faListAlt} />
                  </div>
                  Viết bài
                  <div className="sb-sidenav-collapse-arrow">
                    <i className="fas fa-angle-down"></i>
                  </div>
                </a>
              </div>
            </div>
            <div className="sb-sidenav-footer">
              <div className="small">Logged in as:</div>
              Start Bootstrap
            </div>
          </nav>
        </div>
        <div id="layoutSidenav_content">
          <main></main>
          <footer className="py-4 bg-light mt-auto">
            <div className="container-fluid">
              <div className="d-flex align-items-center justify-content-between small">
                <div className="text-muted">
                  Copyright &copy; Your Website 2020
                </div>
                <div>
                  <a href="index.html">Privacy Policy</a>
                  &middot;
                  <a href="index.html">Terms &amp; Conditions</a>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
