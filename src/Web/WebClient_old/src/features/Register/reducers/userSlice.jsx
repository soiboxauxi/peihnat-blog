import { createSlice } from "@reduxjs/toolkit";
import { userConstants } from "../../../constants/user.constants";

const userSlice = createSlice({
  name: "user",
  initialState: { success: false, data: {} },
  reducers: {
    register: (state, action) => {
      return action.payload;
    },
    request: (state, action) => {
      return { type: userConstants.REGISTER_REQUEST, user: action.payload };
    },
    success: (state, action) => {
      return { type: userConstants.REGISTER_SUCCESS, user: action.payload };
    },
    failure: (state, action) => {
      return { type: userConstants.REGISTER_FAILURE, error: action.payload };
    },
  },
  extraReducers: {},
});

const { reducer, actions } = userSlice;
export const { register, request, success, failure } = actions;
export default reducer;
