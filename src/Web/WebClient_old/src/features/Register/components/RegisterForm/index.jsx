import React from "react";
import PropTypes from "prop-types";
import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import InputField from "../../../../custom-fields/InputField/index.jsx";

RegisterForm.propTypes = {
  onSubmit: PropTypes.func,
};

RegisterForm.defaultProps = {
  onSubmit: null,
};

function RegisterForm(props) {
  const { initialValues } = props;

  const validationSchema = Yup.object().shape({});

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={props.onSubmit}
    >
      {(formikProps) => {
        return (
          <Form>
            <FastField
              name="inputEmailAddress"
              component={InputField}
              label="Email"
              type="email"
              placeholder="Enter email address"
            />
            <div className="form-row">
              <div className="col-md-6">
                <FastField
                  name="inputPassword"
                  component={InputField}
                  label="Password"
                  type="password"
                  placeholder="Enter password"
                />
              </div>
              <div className="col-md-6">
                <FastField
                  name="inputConfirmPassword"
                  component={InputField}
                  label="Confirm Password"
                  type="inputConfirmPassword"
                  placeholder="Confirm password"
                />
              </div>
            </div>
            <div className="form-group mt-4 mb-0">
              <button className="btn btn-primary btn-block" type="submit">
                Create Account
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default RegisterForm;
